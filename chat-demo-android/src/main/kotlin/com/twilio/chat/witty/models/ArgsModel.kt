package com.twilio.chat.witty.models

data class ArgsModel(val board_id: Int, val column_id:Int)
