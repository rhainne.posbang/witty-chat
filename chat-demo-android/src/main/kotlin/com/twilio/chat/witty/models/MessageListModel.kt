package com.twilio.chat.witty.models

import com.twilio.chat.witty.ChannelModel
import com.twilio.chat.witty.activities.MessageActivity
import com.twilio.chat.Message

data class MessageListModel(val message: MessageActivity.MessageItem, val group : Boolean)
