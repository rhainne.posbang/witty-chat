package com.twilio.chat.witty.views

import android.content.Context
import android.util.Log
import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.Glide
import com.twilio.chat.witty.Globals
import com.twilio.chat.witty.R
import com.twilio.chat.witty.models.ContactsDetailsModel
import de.hdodenhof.circleimageview.CircleImageView
import eu.inloop.simplerecycleradapter.SettableViewHolder
import kotterknife.bindView
import org.jetbrains.anko.AnkoLogger

class NewMessageViewHolder : SettableViewHolder<ContactsDetailsModel>, AnkoLogger {
    val contactName: TextView by bindView(R.id.contact_name)
    val contactEmail: TextView by bindView(R.id.contact_email)
    val contactProfileImage: CircleImageView by bindView(R.id.contact_profile_image)

    constructor(context: Context, parent: ViewGroup)
            : super(context, R.layout.contacts_item_layoutl, parent) {
    }

    override fun setData(employee: ContactsDetailsModel) {
        var firstName = employee.first_name ?: ""
        var middleName = employee.middle_initial ?: ""
        var lastName = employee.last_name ?: ""
        var email = employee.email ?: ""
        var imagePathFinal = ""

        if (employee.image_path!!.contains(".com")) {
            imagePathFinal = "https:${employee.image_path}"
        } else imagePathFinal = "https://${Globals.currentEmployeeDetails.organization_slug}.wittymanager.com${employee.image_path}"

        Glide.with(itemView.context).load("$imagePathFinal").placeholder(R.drawable.user_avatar).dontAnimate().into(contactProfileImage)

        Log.e("image path", "$imagePathFinal")

        contactName.text = "$firstName $middleName $lastName"
        contactEmail.text = email
    }
}
