package com.twilio.chat.witty.models

data class ChatCommandDetailResponse(val command_response: Int, val board_id:Int, val column_id:Int, var success : Boolean, var card : CardResponseModel)
