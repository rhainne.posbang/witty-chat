package com.twilio.chat.witty.models

data class CreateTicketResponseModel(val success: Boolean,val err_message: String,val last_inserted_id: Int,val last_query: String,val series_no: String)
