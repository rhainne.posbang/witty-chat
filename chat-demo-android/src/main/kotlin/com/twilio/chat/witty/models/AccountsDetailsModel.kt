package com.twilio.chat.witty.models

import com.twilio.chat.witty.ChannelModel

data class AccountsDetailsModel(val id: Int, val account_id : Int, val user_id:Int, val account_type_id:Int, val industry_type_id:Int, val business_type_id:Int, val code :String, val name :String, val email :String)
