package com.twilio.chat.witty.models

data class UserOrgModel(val success: Boolean, val record: ArrayList<UserOrgDetailsModel>, val total:Int)
