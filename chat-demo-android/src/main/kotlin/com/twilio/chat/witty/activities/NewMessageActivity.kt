package com.twilio.chat.witty.activities

import ChatCallbackListener
import ToastStatusListener
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import com.cysion.wedialog.WeDialog
import com.mrntlu.toastie.Toastie
import com.twilio.chat.*
import com.twilio.chat.witty.*
import com.twilio.chat.witty.R
import com.twilio.chat.witty.api.RetrofitClient
import com.twilio.chat.witty.models.ContactsDetailsModel
import com.twilio.chat.witty.models.ContactsModel
import com.twilio.chat.witty.models.CurrentEmployeeDetailsModel
import com.twilio.chat.witty.views.NewMessageViewHolder
import eu.inloop.simplerecycleradapter.ItemClickListener
import eu.inloop.simplerecycleradapter.SettableViewHolder
import eu.inloop.simplerecycleradapter.SimpleRecyclerAdapter
import kotlinx.android.synthetic.main.activity_new_message.*
import org.jetbrains.anko.startActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


class NewMessageActivity : AppCompatActivity(), ItemClickListener<ContactsDetailsModel> {

    private lateinit var adapter: SimpleRecyclerAdapter<ContactsDetailsModel>
    private lateinit var basicClient: BasicChatClient
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_message)

        basicClient = TwilioApplication.instance.basicClient

        backBtn.setOnClickListener {

            super.onBackPressed()
        }

        adapter = SimpleRecyclerAdapter(this,
                object : SimpleRecyclerAdapter.CreateViewHolder<ContactsDetailsModel>() {
                    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettableViewHolder<ContactsDetailsModel> {
                        return NewMessageViewHolder(this@NewMessageActivity, parent)
                    }
                })

        getEmployees(Globals.currentEmployeeDetails.org_id)

        newMessageSearch.doAfterTextChanged {
            filterContactsList()
        }
    }

    fun getEmployees(org_id: Int) {

        WeDialog.loading(this@NewMessageActivity)
        RetrofitClient.getContacts(org_id, object : Callback<ContactsModel> {

            override fun onResponse(call: Call<ContactsModel>, response: Response<ContactsModel>) {

                val responseModel = response.body()

                when {
                    response.isSuccessful && responseModel != null -> {
                        WeDialog.dismiss()

                        Globals.contactList = responseModel.record

                        initData()

                        new_message_contacts_list.adapter = adapter

                        new_message_contacts_list.layoutManager = LinearLayoutManager(this@NewMessageActivity).apply {
                            orientation = LinearLayoutManager.VERTICAL
                        }

                    }
                    else -> {
                        Toastie.warning(this@NewMessageActivity, "Error when getting list of employees", Toast.LENGTH_SHORT).show()
                        WeDialog.dismiss()
                    }
                }
            }

            override fun onFailure(call: Call<ContactsModel>, t: Throwable) {

                Toastie.error(this@NewMessageActivity, "Cannot Connect To Server", Toast.LENGTH_SHORT).show()
                Log.e("Employee List Error", "" + t.message)
                WeDialog.dismiss()
            }
        })
    }

    private fun filterContactsList() {

        var filteredList: ArrayList<ContactsDetailsModel> = ArrayList()
        filteredList.clear()
        Globals.contactList.forEach {
            if (it.middle_initial != null && it.last_name != null) {

                if (it.first_name!!.toLowerCase()!!.contains(newMessageSearch.text.toString().toLowerCase()) || it.middle_initial!!.toLowerCase()!!.contains(newMessageSearch.text.toString().toLowerCase()) || it.last_name!!.toLowerCase()!!.contains(newMessageSearch.text.toString().toLowerCase())) {
                    filteredList.add(it)
                }
            } else if (it.middle_initial == null && it.last_name != null) {

                if (it.first_name!!.toLowerCase()!!.contains(newMessageSearch.text.toString().toLowerCase()) || it.last_name!!.toLowerCase()!!.contains(newMessageSearch.text.toString().toLowerCase())) {
                    filteredList.add(it)
                }

            } else {
                if (it.first_name!!.toLowerCase()!!.contains(newMessageSearch.text.toString().toLowerCase())) {
                    filteredList.add(it)
                }
            }
            adapter.clear()
            adapter.addItems(filteredList)
            adapter.notifyDataSetChanged()
        }
    }

    private fun initData() {

        Collections.sort(Globals.contactList, Comparator { v1, v2 -> v1.first_name!!.compareTo(v2.first_name.toString()) })

        Globals.contactList.forEach {
            if (it.email != Globals.currentEmployeeDetails.email) {
                adapter.addItem(it)
            }
        }
        adapter.notifyDataSetChanged()
    }

    override fun onItemClick(item: ContactsDetailsModel, viewHolder: SettableViewHolder<ContactsDetailsModel>, view: View) {
        checkIfChannelExists(Globals.currentEmployeeDetails.email.toString(), item.email.toString(), false)
    }

    fun checkIfChannelExists(user1: String, user2: String, isNewChannel: Boolean) {
        WeDialog.loading(this)

        basicClient = TwilioApplication.instance.basicClient

        basicClient.chatClient?.channels?.getUserChannelsList(object : CallbackListener<Paginator<ChannelDescriptor>>() {
            override fun onSuccess(channelDescriptorPaginator: Paginator<ChannelDescriptor>) {
                if (channelDescriptorPaginator.items.any { channels -> channels.uniqueName == "$user1-$user2" || "$user2-$user1" == channels.uniqueName }) {
                    channelDescriptorPaginator.items.forEach {

                        if (it.uniqueName == "$user1-$user2" || "$user2-$user1" == it.uniqueName) {
                            if (isNewChannel) {
                                openChannel(ChannelModel(it), user1, user2, true)
                            } else openChannel(ChannelModel(it), user1, user2, false)

                        }
                    }

                } else {
                    getChannel(user1, user2)
                }
            }
        })

    }

    fun getChannel(user1: String, user2: String) {
        basicClient = TwilioApplication.instance.basicClient

        basicClient.chatClient?.channels?.getChannel("$user1-$user2", object : CallbackListener<Channel>() {
            override fun onSuccess(channel: Channel) {
                channel!!.join(null)
                openChannel(ChannelModel(channel), user1, user2, false)

            }

            override fun onError(errorInfo: ErrorInfo?) {

                basicClient.chatClient?.channels?.getChannel("$user2-$user1", object : CallbackListener<Channel>() {
                    override fun onSuccess(channel: Channel) {
                        channel!!.join(null)
                        openChannel(ChannelModel(channel), user1, user2, false)

                    }

                    override fun onError(errorInfo: ErrorInfo?) {
                        createChannel(user1, user2)
                    }
                })
            }
        })
    }

    fun createChannel(user1: String, user2: String) {
        basicClient = TwilioApplication.instance.basicClient

        val builder = basicClient.chatClient?.channels?.channelBuilder()

        Log.e("creating channel", "$user1-$user2")

        builder?.withFriendlyName("")
                ?.withUniqueName("$user1-$user2")
                ?.withType(Channel.ChannelType.PRIVATE)
                ?.build(object : CallbackListener<Channel>() {
                    override fun onSuccess(newChannel: Channel) {

                        checkIfChannelExists(user1, user2, true)

                    }

                    override fun onError(errorInfo: ErrorInfo?) {
                        Log.e("Channel Creation error", "" + errorInfo)
                    }
                })
    }


    fun openChannel(channel: ChannelModel, user1: String, user2: String, isNewChannel: Boolean) {
        if (isNewChannel) {
            channel.getChannel(ChatCallbackListener<Channel>() {
                it!!.join(
                        ToastStatusListener("Successfully joined channel",
                                "Failed to join channel") {
                        })

                it!!.members.addByIdentity(user2, ToastStatusListener(
                        "Successful addByIdentity",
                        "Error adding member"))

                Log.e("user2", "$user2")

            })

        }

        Handler().postDelayed({
            channel.getChannel(ChatCallbackListener<Channel>() {
                it!!.join(
                        ToastStatusListener("Successfully joined channel",
                                "Failed to join channel") {
                        })

                WeDialog.dismiss()

                startActivity<MessageActivity>(
                        Constants.EXTRA_CHANNEL to it,
                        Constants.EXTRA_CHANNEL_SID to it.sid
                )
            })
        }, 0)

    }


}
