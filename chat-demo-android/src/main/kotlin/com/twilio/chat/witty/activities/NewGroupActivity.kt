package com.twilio.chat.witty.activities

import ChatCallbackListener
import ToastStatusListener
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import com.cysion.wedialog.WeDialog
import com.mrntlu.toastie.Toastie
import com.twilio.chat.*
import com.twilio.chat.witty.*
import com.twilio.chat.witty.R
import com.twilio.chat.witty.api.RetrofitClient
import com.twilio.chat.witty.models.ContactsDetailsModel
import com.twilio.chat.witty.models.ContactsModel
import com.twilio.chat.witty.views.AddedToGroupViewHolder
import com.twilio.chat.witty.views.ChannelViewHolder
import com.twilio.chat.witty.views.NewMessageViewHolder
import eu.inloop.simplerecycleradapter.ItemClickListener
import eu.inloop.simplerecycleradapter.SettableViewHolder
import eu.inloop.simplerecycleradapter.SimpleRecyclerAdapter
import kotlinx.android.synthetic.main.activity_channel.*
import kotlinx.android.synthetic.main.activity_new_group.*
import kotlinx.android.synthetic.main.activity_new_message.*
import kotlinx.android.synthetic.main.activity_new_message.backBtn
import kotlinx.android.synthetic.main.activity_new_message.newMessageSearch
import kotlinx.android.synthetic.main.activity_new_message.new_message_contacts_list
import kotlinx.android.synthetic.main.added_to_group_item_layout.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.custom.forEachReversed
import org.jetbrains.anko.startActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


class NewGroupActivity : AppCompatActivity(), ItemClickListener<ContactsDetailsModel> {

    private lateinit var adapter: SimpleRecyclerAdapter<ContactsDetailsModel>
    private lateinit var adapter2: SimpleRecyclerAdapter<ContactsDetailsModel>
    private lateinit var basicClient: BasicChatClient
    var newGroupList: ArrayList<ContactsDetailsModel> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_group)

        basicClient = TwilioApplication.instance.basicClient

        backBtn.setOnClickListener {

            super.onBackPressed()
        }

        createGroupBtn.setOnClickListener {
            if(newGroupList.isNotEmpty()){
                WeDialog.loading(this@NewGroupActivity)
                createAndJoinChannel()
            }else  Toastie.error(this@NewGroupActivity, "You Cannot Create a group by yourself", Toast.LENGTH_SHORT).show()

        }

        adapter = SimpleRecyclerAdapter(this,
                object : SimpleRecyclerAdapter.CreateViewHolder<ContactsDetailsModel>() {
                    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettableViewHolder<ContactsDetailsModel> {
                        return NewMessageViewHolder(this@NewGroupActivity, parent)
                    }
                })



        adapter2 = SimpleRecyclerAdapter(
                { item: ContactsDetailsModel, _, view ->
                    newGroupList.remove(item)
                    adapter2.removeItem(item)
                    adapter2.notifyDataSetChanged()
                    if (newGroupList.isEmpty()) {
                        added_to_group_list_layout.visibility = View.GONE
                        adapter2.notifyDataSetChanged()
                    }

                },
                object : SimpleRecyclerAdapter.CreateViewHolder<ContactsDetailsModel>() {
                    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettableViewHolder<ContactsDetailsModel> {
                        return AddedToGroupViewHolder(this@NewGroupActivity, parent)
                    }
                })

        getEmployees(Globals.currentEmployeeDetails.org_id)

        added_to_group_list.adapter = adapter2

        added_to_group_list.layoutManager = LinearLayoutManager(this).apply {
            orientation = LinearLayoutManager.HORIZONTAL
        }


        newMessageSearch.doAfterTextChanged {
            filterContactsList()
        }
    }

    fun getEmployees(org_id: Int) {

        WeDialog.loading(this@NewGroupActivity)
        RetrofitClient.getContacts(org_id, object : Callback<ContactsModel> {

            override fun onResponse(call: Call<ContactsModel>, response: Response<ContactsModel>) {

                val responseModel = response.body()

                when {
                    response.isSuccessful && responseModel != null -> {
                        WeDialog.dismiss()

                        Globals.contactList = responseModel.record

                        initData()

                        new_group_contacts_list.adapter = adapter

                        new_group_contacts_list.layoutManager = LinearLayoutManager(this@NewGroupActivity).apply {
                            orientation = LinearLayoutManager.VERTICAL
                        }

                    }
                    else -> {
                        Toastie.warning(this@NewGroupActivity, "Error when getting list of employees", Toast.LENGTH_SHORT).show()
                        WeDialog.dismiss()
                    }
                }
            }

            override fun onFailure(call: Call<ContactsModel>, t: Throwable) {

                Toastie.error(this@NewGroupActivity, "Cannot Connect To Server", Toast.LENGTH_SHORT).show()
                Log.e("Employee List Error", "" + t.message)
                WeDialog.dismiss()
            }
        })
    }
    private fun filterContactsList() {

        var filteredList: ArrayList<ContactsDetailsModel> = ArrayList()
        filteredList.clear()
        Globals.contactList.forEach {
            if (it.middle_initial != null && it.last_name != null) {

                if (it.first_name!!.toLowerCase()!!.contains(newMessageSearch.text.toString().toLowerCase()) || it.middle_initial!!.toLowerCase()!!.contains(newMessageSearch.text.toString().toLowerCase()) || it.last_name!!.toLowerCase()!!.contains(newMessageSearch.text.toString().toLowerCase())) {
                    filteredList.add(it)
                }
            } else if (it.middle_initial == null && it.last_name != null) {

                if (it.first_name!!.toLowerCase()!!.contains(newMessageSearch.text.toString().toLowerCase()) || it.last_name!!.toLowerCase()!!.contains(newMessageSearch.text.toString().toLowerCase())) {
                    filteredList.add(it)
                }

            } else {
                if (it.first_name!!.toLowerCase()!!.contains(newMessageSearch.text.toString().toLowerCase())) {
                    filteredList.add(it)
                }
            }
            adapter.clear()
            adapter.addItems(filteredList)
            adapter.notifyDataSetChanged()
        }
    }

    private fun initData() {
        Collections.sort(Globals.contactList, Comparator { v1, v2 -> v1.first_name!!.compareTo(v2.first_name.toString()) })

        Globals.contactList.forEach {
            if (it.email != Globals.currentEmployeeDetails.email) {
                adapter.addItem(it)
            }
        }
        adapter.notifyDataSetChanged()

        adapter2.notifyDataSetChanged()

    }

    override fun onItemClick(item: ContactsDetailsModel, viewHolder: SettableViewHolder<ContactsDetailsModel>, view: View) {
        if (newGroupList.isEmpty()) {
            newGroupList.add(item)
            adapter2.addItem(item)
            adapter2.notifyDataSetChanged()
            added_to_group_list_layout.visibility = View.VISIBLE
        } else if (newGroupList.any { it.email != item.email }) {
            newGroupList.add(item)
            adapter2.addItem(item)
            adapter2.notifyDataSetChanged()

        }

    }

    fun createAndJoinChannel() {

        basicClient = TwilioApplication.instance.basicClient

        val builder = basicClient.chatClient?.channels?.channelBuilder()

        var groupName: String = ""

        newGroupList.forEach {
            var firstName =it.first_name ?: ""
            var lastName = it?.last_name ?: ""

            if (groupName == "") {
                groupName += "$firstName"
            } else {
                groupName += ",$firstName"
            }
        }

        Log.e("groupName",""+groupName)
        builder?.withFriendlyName(groupName)
                ?.withType(Channel.ChannelType.PRIVATE)
                ?.build(object : CallbackListener<Channel>() {
                    override fun onSuccess(newChannel: Channel) {

                        newChannel!!.join(
                                ToastStatusListener("Successfully joined channel",
                                        "Failed to join channel") {
                                })

                        newGroupList.forEach {
                            newChannel!!.members.addByIdentity(it.email, ToastStatusListener(
                                    "Successful addByIdentity",
                                    "Error adding ${it.email} "))
                        }

                        Handler().postDelayed({
                                WeDialog.dismiss()

                                startActivity<MessageActivity>(
                                        Constants.EXTRA_CHANNEL to newChannel,
                                        Constants.EXTRA_CHANNEL_SID to newChannel.sid
                                )

                        }, 0)

                    }

                    override fun onError(errorInfo: ErrorInfo?) {
                        Log.e("Channel Creation error", "" + errorInfo)
                    }
                })
    }


}
