package com.twilio.chat.witty.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.twilio.chat.witty.Converters
import com.twilio.chat.witty.dao.DBDao
import com.twilio.chat.witty.models.CurrentEmployeeDetailsModel
import com.twilio.chat.witty.models.ContactsDetailsModel


@Database(entities = arrayOf(CurrentEmployeeDetailsModel::class, ContactsDetailsModel::class), version = 1)
@TypeConverters(Converters::class)
abstract class RoomDatabase : RoomDatabase() {
    abstract fun dbDao(): DBDao
}
