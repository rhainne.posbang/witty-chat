package com.twilio.chat.witty.models

data class DeepLinkContactModel(val email: String?)
