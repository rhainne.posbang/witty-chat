package com.twilio.chat.witty.api


import com.twilio.chat.witty.models.*
import retrofit2.Call
import retrofit2.http.*

interface ApiManager {

    @FormUrlEncoded
    @POST("v1/user/login")
    fun loginUser(@Field("user_name") username: String?,
                  @Field("password") password: String?): Call<LoginModel>

    @GET("v2/employees/employee")
    fun getCurrentEmployee(@Query("id") id: Int): Call<CurrentEmployeeModel>

    /*@GET("v2/user/contacts/get_org")
    fun getContacts(@Query("org_id") organization_id: Int, @Query("limit") limit: Int): Call<ContactsModel>*/

    @GET("v2/employees/employees")
    fun getContacts(@Query("organization_id") organization_id: Int): Call<ContactsModel>

    @FormUrlEncoded
    @POST("v2/chat/command")
    fun chatCommand(@Field("org_id") org_id: Int, @Field("employee_id") employee_id: Int,
                    @Field("user_id") user_id: Int, @Field("command") command: String, @Field("args") args: String): Call<ChatCommandResponseModel>


    @GET("v2/board/get")
    fun getBoards(@Query("org_id") organization_id: Int, @Query("limit") limit: Int): Call<BoardsModel>

    @GET("v1/organization")
    fun getOrg(@Query("id") id: Int): Call<OrgModel>

    @GET("v1/user/org")
    fun getUserOrg(@Query("user_id") user_id: Int): Call<UserOrgModel>

    @GET("v1/account/list")
    fun getAccounts (@Query("org_id") org_id: Int): Call<ArrayList<AccountsDetailsModel>>

    @FormUrlEncoded
    @POST("v1/ticket/quickcreate")
    fun createTicket(@Field("org_id") org_id: Int, @Field("account_id") account_id: Int,
                    @Field("date_start") date_start: String, @Field("date_due") date_due: String, @Field("ticket_type_id") ticket_type_id: Int, @Field("title") title: String, @Field("created_by") created_by: Int,@Field("description") description: String): Call<CreateTicketResponseModel>

}