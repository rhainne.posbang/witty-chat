package com.twilio.chat.witty.activities

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import com.andrefrsousa.superbottomsheet.SuperBottomSheetFragment
import com.cysion.wedialog.WeDialog
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.gson.Gson
import com.mrntlu.toastie.Toastie
import com.twilio.chat.ChatClient
import com.twilio.chat.witty.BasicChatClient.LoginListener
import com.twilio.chat.witty.BuildConfig
import com.twilio.chat.witty.Globals
import com.twilio.chat.witty.R
import com.twilio.chat.witty.TwilioApplication
import com.twilio.chat.witty.api.RetrofitClient
import com.twilio.chat.witty.database.RoomDatabase
import com.twilio.chat.witty.models.*
import com.twilio.chat.witty.services.RegistrationIntentService
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.login
import kotlinx.android.synthetic.main.activity_login_new.*
import kotlinx.android.synthetic.main.bottom_select_org_panel_layout.view.*
import org.jetbrains.anko.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginActivity : AppCompatActivity(), LoginListener, AnkoLogger {

    private lateinit var roomDatabase: RoomDatabase

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        val deepLink = intent?.data
        val data = deepLink?.getQueryParameter("data")
        val parsedData = Gson().fromJson(data.toString(), DeepLinkModel::class.java)
        roomDatabase = Room.databaseBuilder(applicationContext, RoomDatabase::class.java, "maindb").allowMainThreadQueries().build()
        var currentEmployee: CurrentEmployeeDetailsModel = roomDatabase.dbDao().getCurrentEmployee()
        var allEmployees: List<ContactsDetailsModel> = roomDatabase.dbDao().getAllEmployees()
        Globals.deepLinkSelectedUser = parsedData?.selected_contact?.email.toString()

        if (deepLink != null) {
            if (roomDatabase.dbDao().checkIfAllEmployeesExists() != 0) {
                if (parsedData != null) {
                    if (currentEmployee.email == parsedData?.email) {
                        Globals.currentEmployeeDetails = currentEmployee
                        Globals.contactList = allEmployees

                        startActivity(Intent(applicationContext, PlaceHolderActivity::class.java))
                    } else {
                        var basicClient = TwilioApplication.instance.basicClient
                        basicClient.shutdown()

                        Globals.contactList.forEach {
                            roomDatabase.dbDao().deleteAllEmployees(it)
                        }
                        roomDatabase.dbDao().deleteCurrentUser(Globals.currentEmployeeDetails)

                        username.setText(parsedData?.username.toString())
                        password.setText(parsedData?.password.toString())
                        loginUser()
                    }
                }
            } else {
                username.setText(parsedData?.username.toString())
                password.setText(parsedData?.password.toString())
                loginUser()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_new)

        val mVersionName = BuildConfig.VERSION_NAME
        login_versionnum.text = "Version " + mVersionName

        //datebase
        roomDatabase = Room.databaseBuilder(applicationContext, RoomDatabase::class.java, "maindb").allowMainThreadQueries().build()
        var currentEmployee: CurrentEmployeeDetailsModel = roomDatabase.dbDao().getCurrentEmployee()
        var allEmployees: List<ContactsDetailsModel> = roomDatabase.dbDao().getAllEmployees()

        val intent = intent
        val deepLink = intent.data
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        // check if deeplink
        if (deepLink != null) {
            val data = deepLink.getQueryParameter("data")

            val parsedData = Gson().fromJson(data.toString(), DeepLinkModel::class.java)

            Globals.deepLinkSelectedUser = parsedData?.selected_contact?.email.toString()

            if (roomDatabase.dbDao().checkIfAllEmployeesExists() != 0) {
                if (currentEmployee.email == parsedData?.email) {

                    Globals.currentEmployeeDetails = currentEmployee
                    Globals.contactList = allEmployees

                    startActivity(Intent(applicationContext, PlaceHolderActivity::class.java))

                } else {

                    var basicClient = TwilioApplication.instance.basicClient
                    basicClient.shutdown()

                    Globals.contactList.forEach {
                        roomDatabase.dbDao().deleteAllEmployees(it)
                    }
                    roomDatabase.dbDao().deleteCurrentUser(Globals.currentEmployeeDetails)

                    username.setText(parsedData?.username.toString())
                    password.setText(parsedData?.password.toString())
                    loginUser()
                }
            } else {
                username.setText(parsedData?.username.toString())
                password.setText(parsedData?.password.toString())
                loginUser()
            }

        } else {

            //check if previous login exists
            if (roomDatabase.dbDao().checkIfAllEmployeesExists() != 0) {

                Globals.currentEmployeeDetails = currentEmployee
                Globals.contactList = allEmployees

                startActivity(Intent(applicationContext, PlaceHolderActivity::class.java))
            }

        }

        login.setOnClickListener {
            loginUser()
        }


    }


    fun loginUser() {
        WeDialog.loading(this)

        RetrofitClient.postLogin(username.text.toString(), password.text.toString(), object : Callback<LoginModel> {

            override fun onResponse(call: Call<LoginModel>, response: Response<LoginModel>) {

                val responseModel = response.body()

                when {
                    response.isSuccessful && responseModel?.user_name != null -> {

                        Log.e("user", "" + responseModel.user_name)
                        Log.e("employee id", "" + responseModel.employee_id)

                        getCurrentEmployeeDetails(responseModel.employee_id)

                    }
                    else -> {
                        Toastie.warning(this@LoginActivity, "" + responseModel?.restriction_msg, Toast.LENGTH_SHORT).show()
                        WeDialog.dismiss()
                    }
                }
            }

            override fun onFailure(call: Call<LoginModel>, t: Throwable) {

                Toastie.error(this@LoginActivity, "Cannot Connect To Server", Toast.LENGTH_SHORT).show()
                Log.e("Login Error", "" + t.message)
                WeDialog.dismiss()
            }
        })
    }

    fun getCurrentEmployeeDetails(employee_id: Int) {

        Log.e("employeeid", "" + employee_id)


        RetrofitClient.getCurrentEmployee(employee_id, object : Callback<CurrentEmployeeModel> {

            override fun onResponse(call: Call<CurrentEmployeeModel>, response: Response<CurrentEmployeeModel>) {

                val responseModel = response.body()

                when {
                    response.isSuccessful && responseModel != null -> {

                        Globals.currentEmployeeDetails = responseModel.record
                        Log.e("userid", "" + responseModel.record.id)
                        Log.e("email", "" + responseModel.record.email)

                        Globals.currentEmployeeID = employee_id

                        getUserOrg(responseModel.record.user_id, responseModel.record.email.toString())


                    }
                    else -> {
                        Toastie.warning(this@LoginActivity, "Error when getting employee details", Toast.LENGTH_SHORT).show()
                        WeDialog.dismiss()
                    }
                }
            }

            override fun onFailure(call: Call<CurrentEmployeeModel>, t: Throwable) {

                Toastie.error(this@LoginActivity, "Cannot Connect To Server", Toast.LENGTH_SHORT).show()
                Log.e("Current Employee Error", "" + t.message)
                WeDialog.dismiss()
            }
        })
    }

    fun getUserOrg(user_id: Int, email: String) {

        RetrofitClient.getUserOrg(user_id, object : Callback<UserOrgModel> {

            override fun onResponse(call: Call<UserOrgModel>, response: Response<UserOrgModel>) {

                val responseModel = response.body()

                when {
                    response.isSuccessful && responseModel != null -> {
                        WeDialog.dismiss()

                        if (responseModel.total > 1) {
                            val sheet = SelectOrgSheetFragment(responseModel.record, email,this@LoginActivity)
                            sheet.show(supportFragmentManager, "Select Org")
                        } else getEmployees(responseModel.record[0].org_id, email)


                    }
                    else -> {
                        Toastie.warning(this@LoginActivity, "Error getting user org", Toast.LENGTH_SHORT).show()
                        WeDialog.dismiss()
                    }
                }
            }

            override fun onFailure(call: Call<UserOrgModel>, t: Throwable) {

                Toastie.error(this@LoginActivity, "Cannot Connect To Server", Toast.LENGTH_SHORT).show()
                Log.e("User Org Error", "" + t.message)
                WeDialog.dismiss()
            }
        })
    }


    fun getEmployees(org_id: Int, email: String) {
        WeDialog.loading(this@LoginActivity)
        RetrofitClient.getContacts(org_id, object : Callback<ContactsModel> {

            override fun onResponse(call: Call<ContactsModel>, response: Response<ContactsModel>) {

                val responseModel = response.body()

                when {
                    response.isSuccessful && responseModel != null -> {

                        Globals.contactList = responseModel.record
                        Globals.selectedOrg = org_id

                        loginTwilio(email)

                    }
                    else -> {
                        Toastie.warning(this@LoginActivity, "Error when getting list of employees", Toast.LENGTH_SHORT).show()
                        WeDialog.dismiss()
                    }
                }
            }

            override fun onFailure(call: Call<ContactsModel>, t: Throwable) {

                Toastie.error(this@LoginActivity, "Cannot Connect To Server", Toast.LENGTH_SHORT).show()
                Log.e("Employee List Error", "" + t.message)
                WeDialog.dismiss()
            }
        })
    }

    fun loginTwilio(email: String) {

        // val certPinningChosen = certPinning.isChecked
        val certPinningChosen = true
        //val realm = realmSelect.selectedItem as String
        val realm = DEFAULT_REALM
        // val ttl = tokenTtlTextBox.text.toString()
        val ttl = DEFAULT_TTL

        /*val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        sharedPreferences.edit()
                .putString("userName", email)
                .putBoolean("pinCerts", certPinningChosen)
                .putString("realm", realm)
                .putString("ttl", ttl)
                .apply()*/

        val url = Uri.parse(BuildConfig.ACCESS_TOKEN_SERVICE_URL)
                .buildUpon()
                .appendQueryParameter("identity", email)
                .appendQueryParameter("realm", realm)
                .appendQueryParameter("ttl", ttl)
                .build()
                .toString()
        debug { "url string : $url" }

        TwilioApplication.instance.basicClient.login(email, certPinningChosen, realm, url, this@LoginActivity)

    }

    override fun onResume() {
        super.onResume()
        /*val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        val userName = sharedPreferences.getString("userName", DEFAULT_CLIENT_NAME)
        val certPin = sharedPreferences.getBoolean("pinCerts", true)
        val realm = sharedPreferences.getString("realm", DEFAULT_REALM)
        val ttl = sharedPreferences.getString("ttl", DEFAULT_TTL)

        username.setText(userName)*/
        //certPinning.isChecked = certPin
        // realmSelect.setSelection((realmSelect.adapter as ArrayAdapter<String>).getPosition(realm))
        // tokenTtlTextBox.setText(ttl)

        // Make sure no chatclient is created
        if (TwilioApplication.instance.basicClient.chatClient != null) {
            TwilioApplication.instance.basicClient.shutdown()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.login, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_about) {
            showAboutDialog()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showAboutDialog() {
        alert("Version: ${ChatClient.getSdkVersion()}", "About") {
            positiveButton("OK") { dialog -> dialog.cancel() }
        }.show()
    }

    fun setLoginProgressVisible(enable: Boolean) {
        if (!enable) {
            WeDialog.dismiss()
            //loginInputsLayout.visibility = View.GONE
            //loginProgressLayout.visibility = View.VISIBLE
        }
    }

    override fun onLoginStarted() {
        debug { "Log in started" }
        setLoginProgressVisible(true)
    }

    override fun onLoginFinished() {
        setLoginProgressVisible(false)


        if (checkPlayServices()) {
          //fcmAvailable.isChecked = true
            // Start IntentService to register this application with GCM.
            startService<RegistrationIntentService>()
        }

        Globals.contactList.forEach {
            roomDatabase.dbDao().insertAllEmployees(it)
        }

        roomDatabase.dbDao().insertCurrentEmployee(CurrentEmployeeDetailsModel(Globals.currentEmployeeDetails.id,  Globals.selectedOrg, Globals.currentEmployeeDetails.user_id, Globals.currentEmployeeDetails.first_name, Globals.currentEmployeeDetails.last_name,
                Globals.currentEmployeeDetails.middle_initial, Globals.currentEmployeeDetails.organization_name, Globals.currentEmployeeDetails.email, Globals.currentEmployeeDetails.image_path, Globals.currentEmployeeID, Globals.currentEmployeeDetails.organization_slug))


        var currentEmployeeNew: CurrentEmployeeDetailsModel = roomDatabase.dbDao().getCurrentEmployee()

        Globals.currentEmployeeDetails = currentEmployeeNew

        WeDialog.dismiss()
        startActivity<ChannelActivity>()
    }

    override fun onLoginError(errorMessage: String) {
        setLoginProgressVisible(false)
        TwilioApplication.instance.showToast("Error logging in : " + errorMessage, Toast.LENGTH_LONG)
    }

    override fun onLogoutFinished() {
        setLoginProgressVisible(false)
        //TwilioApplication.instance.showToast("Log out finished")
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private fun checkPlayServices(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val resultCode = apiAvailability.isGooglePlayServicesAvailable(this)
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show()
            } else {
                info { "This device is not supported." }
                finish()
            }
            return false
        }
        return true
    }

    companion object {
        private val DEFAULT_CLIENT_NAME = "TestUser"
        private val DEFAULT_REALM = "us1"
        private val DEFAULT_TTL = "3000"
        private val PLAY_SERVICES_RESOLUTION_REQUEST = 9000
    }
}

class SelectOrgSheetFragment(orgList: ArrayList<UserOrgDetailsModel>, email: String,context: Context) : SuperBottomSheetFragment() {
    var og = orgList
    var em = email
    var ctxt = context
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var v = inflater.inflate(R.layout.bottom_select_org_panel_layout, container, false)
        v.selectOrgtitle.text = "Select Organization"

        v.selectOrgSpinner.setTitle("Select Organization")
        v.selectOrgSpinner.setPositiveButton("OK")

        val orgSpinnerList = ArrayList<String>()

        og.forEach {
            orgSpinnerList.add(it.org_name)
        }

        val orgSpinnerAdapter = ArrayAdapter(
                v.context, android.R.layout.simple_spinner_item, orgSpinnerList
        )

        v.selectOrgSpinner.adapter = orgSpinnerAdapter


        v.confirmBtn.setOnClickListener {
            og.forEach {
                if (it.org_name == v.selectOrgSpinner.selectedItem.toString()) {
                    (ctxt as LoginActivity).getEmployees(it.org_id, em)
                    this.dismiss()
                }
            }

        }


        super.onCreateView(inflater, container, savedInstanceState)
        return v
    }

    override fun getStatusBarColor() = Color.RED
    override fun getCornerRadius() = context!!.resources.getDimension(R.dimen.demo_sheet_rounded_corner)
    override fun getExpandedHeight() = -1
}

