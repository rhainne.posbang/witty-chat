package com.twilio.chat.witty.views

import android.content.Context
import android.util.Log
import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.Glide
import com.twilio.chat.witty.Globals
import com.twilio.chat.witty.R
import com.twilio.chat.witty.models.ContactsDetailsModel
import de.hdodenhof.circleimageview.CircleImageView
import eu.inloop.simplerecycleradapter.SettableViewHolder
import kotterknife.bindView
import org.jetbrains.anko.AnkoLogger

class AddedToGroupViewHolder : SettableViewHolder<ContactsDetailsModel>, AnkoLogger {
    val contactName: TextView by bindView(R.id.contact_name)
    val removeBtn: TextView by bindView(R.id.removeContactBtn)

    constructor(context: Context, parent: ViewGroup)
            : super(context, R.layout.added_to_group_item_layout, parent) {
    }

    override fun setData(employee: ContactsDetailsModel) {
        var firstName = employee.first_name ?: ""
        var middleName = employee.middle_initial ?: ""
        var lastName = employee.last_name ?: ""
        var email = employee.email ?: ""
        var imagePathFinal = ""

        contactName.text = "$firstName $middleName $lastName"
    }
}
