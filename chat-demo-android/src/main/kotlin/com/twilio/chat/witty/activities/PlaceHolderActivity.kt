package com.twilio.chat.witty.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.mrntlu.toastie.Toastie
import com.twilio.chat.witty.*
import com.twilio.chat.witty.BuildConfig
import com.twilio.chat.witty.R
import com.twilio.chat.witty.database.RoomDatabase
import com.twilio.chat.witty.models.CurrentEmployeeDetailsModel
import com.twilio.chat.witty.services.RegistrationIntentService
import org.jetbrains.anko.*


class PlaceHolderActivity : AppCompatActivity(), BasicChatClient.LoginListener, AnkoLogger {

    private lateinit var roomDatabase: com.twilio.chat.witty.database.RoomDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_placeholder)

        roomDatabase = Room.databaseBuilder(applicationContext, com.twilio.chat.witty.database.RoomDatabase::class.java, "maindb").allowMainThreadQueries().build()

       // var currentEmployee: CurrentEmployeeDetailsModel = roomDatabase.dbDao().getCurrentEmployee()

        loginTwilio(Globals.currentEmployeeDetails.email.toString())

    }


    fun loginTwilio(email: String) {

        // val certPinningChosen = certPinning.isChecked
        val certPinningChosen = true
        //val realm = realmSelect.selectedItem as String
        val realm = "us1"
        // val ttl = tokenTtlTextBox.text.toString()
        val ttl = "3000"

        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        sharedPreferences.edit()
                .putString("userName", email)
                .putBoolean("pinCerts", certPinningChosen)
                .putString("realm", realm)
                .putString("ttl", ttl)
                .apply()

        val url = Uri.parse(BuildConfig.ACCESS_TOKEN_SERVICE_URL)
                .buildUpon()
                .appendQueryParameter("identity", email)
                .appendQueryParameter("realm", realm)
                .appendQueryParameter("ttl", ttl)
                .build()
                .toString()
        debug { "url string : $url" }

        TwilioApplication.instance.basicClient.login(email, certPinningChosen, realm, url, this@PlaceHolderActivity)

    }


    override fun onLoginStarted() {
        debug { "Log in started" }
    }

    override fun onLoginFinished() {
        startActivity<ChannelActivity>()
    }

    override fun onLoginError(errorMessage: String) {
        var roomDatabase: RoomDatabase = Room.databaseBuilder(applicationContext, RoomDatabase::class.java, "maindb").allowMainThreadQueries().build()
        Globals.contactList.forEach {
            roomDatabase.dbDao().deleteAllEmployees(it)
        }
        roomDatabase.dbDao().deleteCurrentUser(Globals.currentEmployeeDetails)

        startActivity(Intent(applicationContext, LoginActivity::class.java))
        Toastie.error(this@PlaceHolderActivity, "Connection Error Please Check your internet and try again", Toast.LENGTH_LONG).show()
       // TwilioApplication.instance.showToast("Error logging in : " + errorMessage, Toast.LENGTH_LONG)
    }

    override fun onLogoutFinished() {
        //TwilioApplication.instance.showToast("Log out finished")
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private fun checkPlayServices(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val resultCode = apiAvailability.isGooglePlayServicesAvailable(this)
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show()
            } else {
                info { "This device is not supported." }
                finish()
            }
            return false
        }
        return true
    }

    companion object {
        private val DEFAULT_CLIENT_NAME = "TestUser"
        private val DEFAULT_REALM = "us1"
        private val DEFAULT_TTL = "3000"
        private val PLAY_SERVICES_RESOLUTION_REQUEST = 9000
    }


}