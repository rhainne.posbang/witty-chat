package com.twilio.chat.witty.models

data class DeepLinkModel(val username: String?, val password:String? , val email :String? , val selected_contact : DeepLinkContactModel?)
