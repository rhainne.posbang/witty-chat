package com.twilio.chat.witty.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.twilio.chat.witty.models.CurrentEmployeeDetailsModel
import com.twilio.chat.witty.models.ContactsDetailsModel

@Dao
interface DBDao {
    @Query("SELECT * FROM current_employee")
    fun getCurrentEmployee(): CurrentEmployeeDetailsModel

    @Query("SELECT * FROM all_employees")
    fun getAllEmployees(): List<ContactsDetailsModel>

    @Query("SELECT count(*) FROM current_employee")
    fun checkIfCurrentEmployeesExists(): Int

    @Query("SELECT count(*) FROM all_employees")
    fun checkIfAllEmployeesExists(): Int

    @Insert
    fun insertCurrentEmployee(vararg currentEmployee: CurrentEmployeeDetailsModel)

    @Insert
    fun insertAllEmployees(vararg allEmployees: ContactsDetailsModel)

    @Delete
    fun deleteCurrentUser(deleteCurrentUser: CurrentEmployeeDetailsModel)

    @Delete
    fun deleteAllEmployees(deleteAllContacts: ContactsDetailsModel)


}