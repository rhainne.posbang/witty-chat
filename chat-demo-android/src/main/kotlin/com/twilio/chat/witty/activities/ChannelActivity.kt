package com.twilio.chat.witty.activities

import ChatCallbackListener
import ToastStatusListener
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.andrefrsousa.superbottomsheet.SuperBottomSheetFragment
import com.cysion.wedialog.WeDialog
import com.mrntlu.toastie.Toastie
import com.onurkagan.ksnack_lib.Animations.Slide
import com.onurkagan.ksnack_lib.KSnack.KSnack
import com.onurkagan.ksnack_lib.KSnack.KSnackBarEventListener
import com.twilio.chat.*
import com.twilio.chat.Channel.ChannelType
import com.twilio.chat.witty.*
import com.twilio.chat.witty.R
import com.twilio.chat.witty.api.RetrofitClient
import com.twilio.chat.witty.database.RoomDatabase
import com.twilio.chat.witty.models.ChannelListModel
import com.twilio.chat.witty.models.ContactsModel
import com.twilio.chat.witty.utils.Where.CHAT_CLIENT_CPP
import com.twilio.chat.witty.utils.Where.TM_CLIENT_CPP
import com.twilio.chat.witty.utils.simulateCrash
import com.twilio.chat.witty.views.ChannelViewHolder
import eu.inloop.simplerecycleradapter.ItemClickListener
import eu.inloop.simplerecycleradapter.SettableViewHolder
import eu.inloop.simplerecycleradapter.SimpleRecyclerAdapter
import kotlinx.android.synthetic.main.activity_channel.*
import kotlinx.android.synthetic.main.bottom_create_new_panel_layout.view.*
import org.jetbrains.anko.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class ChannelActivity : AppCompatActivity(), ChatClientListener, AnkoLogger, ItemClickListener<ChannelListModel> {
    lateinit var basicClient: BasicChatClient
    private val channels = HashMap<String, ChannelModel>()
    private lateinit var adapter: SimpleRecyclerAdapter<ChannelListModel>
    lateinit var roomDatabase: RoomDatabase


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_channel)

        basicClient = TwilioApplication.instance.basicClient
        basicClient.chatClient?.setListener(this@ChannelActivity)

        roomDatabase = Room.databaseBuilder(applicationContext, RoomDatabase::class.java, "maindb").allowMainThreadQueries().build()

        setupListView()

        sendBtn.setOnClickListener {
            /* FinestWebView.Builder(this).titleDefault("The Finest Artist")
                     .show("http://google.com")*/

            startActivity(Intent(applicationContext, ProfileActivity::class.java))
        }

        newMessageBtn.setOnClickListener {
            val sheet = CreateNewSheetFragment(roomDatabase, basicClient, this)
            sheet.show(supportFragmentManager, "Create New")

        }

        channelSearch.doAfterTextChanged {
            filterChannelList()
        }



    }


    override fun onResume() {
        super.onResume()
         getChannels()
         refreshChannelList()
    }


    fun showNewMessageDialog(channel: Channel, name: String) {
        val kSnack = KSnack(this@ChannelActivity)

        kSnack.setListener(object : KSnackBarEventListener {
            // listener
            override fun showedSnackBar() {
                println("Showed")
            }

            override fun stoppedSnackBar() {
                println("Stopped")
            }
        })
                .setAction("View", View.OnClickListener
                {

                    startActivity<MessageActivity>(
                            Constants.EXTRA_CHANNEL to channel,
                            Constants.EXTRA_CHANNEL_SID to channel.sid
                    )

                })
                .setMessage("$name sent you a message.") // message
                .setTextColor(R.color.we_white) // message text color
                .setBackColor(R.color.textColorSecondary) // background color

                .setButtonTextColor(R.color.we_white) // action button text color
                .setBackgrounDrawable(R.drawable.primary_button) // // background drawable
                .setAnimation(Slide.Up.getAnimation(kSnack.getSnackView()), Slide.Down.getAnimation(kSnack.getSnackView()))
                .setDuration(3000) // you can use for auto close.
                .show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_create_public -> showCreateChannelDialog(ChannelType.PUBLIC)
            R.id.action_create_private -> showCreateChannelDialog(ChannelType.PRIVATE)
            R.id.action_create_public_withoptions -> createChannelWithType(ChannelType.PUBLIC)
            R.id.action_create_private_withoptions -> createChannelWithType(ChannelType.PRIVATE)
            R.id.action_search_by_unique_name -> showSearchChannelDialog()
            R.id.action_user_info -> startActivity(Intent(applicationContext, UserActivity::class.java))
            R.id.action_update_token -> basicClient.updateToken()
            R.id.action_logout -> {
                basicClient.shutdown()
                finish()
            }
            R.id.action_unregistercm -> basicClient.unregisterFcmToken()
            R.id.action_crash_in_java -> throw RuntimeException("Simulated crash in ChannelActivity.kt")
            R.id.action_crash_in_chat_client -> basicClient.chatClient!!.simulateCrash(CHAT_CLIENT_CPP)
            R.id.action_crash_in_tm_client -> basicClient.chatClient!!.simulateCrash(TM_CLIENT_CPP)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun createChannelWithType(type: ChannelType) {
        val rand = Random()
        val value = rand.nextInt(50)

        val attrs = JSONObject()
        try {
            attrs.put("topic", "testing channel creation with options ${value}")
        } catch (xcp: JSONException) {
            error { "JSON exception: $xcp" }
        }

        val typ = if (type == ChannelType.PRIVATE) "Priv" else "Pub"

        val builder = basicClient.chatClient?.channels?.channelBuilder()

        builder?.withFriendlyName("${typ}_TestChannelF_${value}")
                ?.withUniqueName("${typ}_TestChannelU_${value}")
                ?.withType(type)
                ?.withAttributes(Attributes(attrs))
                ?.build(object : CallbackListener<Channel>() {
                    override fun onSuccess(newChannel: Channel) {
                        debug { "Successfully created a channel with options." }
                        channels.put(newChannel.sid, ChannelModel(newChannel))
                        refreshChannelList()
                    }

                    override fun onError(errorInfo: ErrorInfo?) {
                        error { "Error creating a channel" }
                    }
                })
    }

    private fun showCreateChannelDialog(type: ChannelType) {
        alert(R.string.title_add_channel) {
            customView {
                verticalLayout {
                    textView {
                        text = "Enter ${type} name"
                        padding = dip(10)
                    }.lparams(width = matchParent)
                    val channel_name = editText { padding = dip(10) }.lparams(width = matchParent)
                    positiveButton(R.string.create) {
                        val channelName = channel_name.text.toString()
                        Log.e("Channel", "Creating channel with friendly Name|$channelName|")
                        basicClient.chatClient?.channels?.createChannel(channelName, type, ChatCallbackListener<Channel>() {
                            Log.e("Channel", "Channel created with sid|${it.sid}| and type ${it.type}")
                            channels.put(it.sid, ChannelModel(it))
                            refreshChannelList()
                        })
                    }
                    negativeButton(R.string.cancel) {}
                }
            }
        }.show()
    }


    private fun showSearchChannelDialog() {
        alert(R.string.title_find_channel) {
            customView {
                verticalLayout {
                    textView {
                        text = "Enter unique channel name"
                        padding = dip(10)
                    }.lparams(width = matchParent)
                    val channel_name = editText { padding = dip(10) }.lparams(width = matchParent)
                    positiveButton(R.string.search) {
                        val channelSid = channel_name.text.toString()
                        debug { "Searching for ${channelSid}" }
                        basicClient.chatClient?.channels?.getChannel(channelSid, ChatCallbackListener<Channel?>() {
                            if (it != null) {
                                TwilioApplication.instance.showToast("${it.sid}: ${it.friendlyName}")
                            } else {
                                TwilioApplication.instance.showToast("Channel not found.")
                            }
                        })
                    }
                    negativeButton(R.string.cancel) {}
                }
            }
        }.show()
    }

    private fun setupListView() {

        adapter = SimpleRecyclerAdapter(this,
                object : SimpleRecyclerAdapter.CreateViewHolder<ChannelListModel>() {
                    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettableViewHolder<ChannelListModel> {
                        return ChannelViewHolder(this@ChannelActivity, parent)
                    }
                })

        channel_list.adapter = adapter
        channel_list.itemAnimator!!.changeDuration = 0
        channel_list.layoutManager = LinearLayoutManager(this).apply {
            orientation = LinearLayoutManager.VERTICAL

        }

        refreshChannelList()

    }

    override fun onItemClick(item: ChannelListModel, viewHolder: SettableViewHolder<ChannelListModel>, view: View) {
        if (item.channel.status == Channel.ChannelStatus.JOINED) {
            Handler().postDelayed({
                item.channelModel.getChannel(ChatCallbackListener<Channel>() {
                    startActivity<MessageActivity>(
                            Constants.EXTRA_CHANNEL to it,
                            Constants.EXTRA_CHANNEL_SID to it.sid
                    )
                })
            }, 0)
        }
    }


    private fun refreshChannelList() {
        var channelList: ArrayList<ChannelListModel> = ArrayList()
        channels.values.sortedWith(CustomChannelComparator()).forEach { channelListModel ->

            val channelName: List<String> = channelListModel.uniqueName.split("-")

            channelListModel.getChannel(ChatCallbackListener<Channel>() { channelGet ->

                if (channelListModel.type == ChannelType.PRIVATE && channelListModel.uniqueName.contains("-")) {

                    if (channelName[0] != Globals.currentEmployeeDetails.email) {

                        channelList.add(ChannelListModel(channelListModel, channelGet, getEmployeeName(channelName[0]), getImagePath(channelName[0]), false))

                    } else if (channelName[1] != Globals.currentEmployeeDetails.email) {

                        channelList.add(ChannelListModel(channelListModel, channelGet, getEmployeeName(channelName[1]), getImagePath(channelName[1]), false))

                    }
                } else channelList.add(ChannelListModel(channelListModel, channelGet, channelListModel.friendlyName, "", true))
            })
        }
        adapter.clear()
        adapter.addItems(channelList)
        adapter.notifyDataSetChanged()
        //channel_list.scheduleLayoutAnimation()

        //check if from notification
        if(Globals.NotificationUser != ""){
            Globals.deepLinkSelectedUser = Globals.NotificationUser
        }

        //deep link selected user
        Handler().postDelayed({
            if (Globals.deepLinkSelectedUser != "" && channels.isNotEmpty()) {
                if (Globals.contactList.any { it.email == Globals.deepLinkSelectedUser }) {
                    checkIfChannelExists(Globals.currentEmployeeDetails.email.toString(), Globals.deepLinkSelectedUser, false)
                    Globals.deepLinkSelectedUser = ""
                }
            }
        }, 1000)
    }

    fun getEmployeeName(email: String): String {
        var employeName = ""

        if(Globals.contactList.any { it.email==email }){
            Globals.contactList.forEach {
                if (it.email == email) {
                    employeName = "${it.first_name} ${it.last_name}"
                }
            }
        }else{
           employeName = email
        }

        return employeName
    }

    fun getEmployees(org_id: Int) {

        WeDialog.loading(this@ChannelActivity)
        RetrofitClient.getContacts(org_id, object : Callback<ContactsModel> {

            override fun onResponse(call: Call<ContactsModel>, response: Response<ContactsModel>) {

                val responseModel = response.body()

                when {
                    response.isSuccessful && responseModel != null -> {
                        WeDialog.dismiss()

                        Globals.contactList = responseModel.record
                    }
                    else -> {
                        Toastie.warning(this@ChannelActivity, "Error when getting list of employees", Toast.LENGTH_SHORT).show()
                        WeDialog.dismiss()
                    }
                }
            }

            override fun onFailure(call: Call<ContactsModel>, t: Throwable) {
                Toastie.error(this@ChannelActivity, "Cannot Connect To Server", Toast.LENGTH_SHORT).show()
                Log.e("Employee List Error", "" + t.message)
                WeDialog.dismiss()
            }
        })
    }

    fun getImagePath(email: String): String {
        var imagePath = ""
        Globals.contactList.forEach {
            if (it.email == email) {
                imagePath = "${it.image_path}"
            }
        }
        return imagePath
    }


    private fun filterChannelList() {
        var filteredList: ArrayList<ChannelListModel> = ArrayList()
        filteredList.clear()
        channels.values.sortedWith(CustomChannelComparator()).forEach {
            it.getChannel(ChatCallbackListener<Channel>() { channelGet ->

                if (it.type == ChannelType.PRIVATE) {
                    val channelName: List<String> = it.uniqueName.split("-")

                    if (channelName[0] != Globals.currentEmployeeDetails.email) {
                        if (getEmployeeName(channelName[0])!!.toLowerCase()!!.contains(channelSearch.text.toString().toLowerCase())) {
                            filteredList.add(ChannelListModel(it, channelGet, getEmployeeName(channelName[0]), getImagePath(channelName[0]), false))
                        }
                    } else if (channelName[1] != Globals.currentEmployeeDetails.email) {
                        if (getEmployeeName(channelName[1])!!.toLowerCase()!!.contains(channelSearch.text.toString().toLowerCase())) {
                            filteredList.add(ChannelListModel(it, channelGet, getEmployeeName(channelName[1]), getImagePath(channelName[0]), false))
                        }
                    }
                } else {
                    if (it.friendlyName!!.toLowerCase()!!.contains(channelSearch.text.toString().toLowerCase())) {
                        filteredList.add(ChannelListModel(it, channelGet, it.friendlyName, "", true))
                    }
                }

            })
        }
        adapter.clear()
        adapter.addItems(filteredList)
        adapter.notifyDataSetChanged()
        //channel_list.scheduleLayoutAnimation()

    }

    private fun getChannelsPage(paginator: Paginator<ChannelDescriptor>) {
        for (cd in paginator.items) {
            error { "Adding channel descriptor for sid|${cd.sid}| friendlyName ${cd.friendlyName}" }
            if (cd.status == Channel.ChannelStatus.JOINED || cd.status == Channel.ChannelStatus.INVITED) {
                channels.put(cd.sid, ChannelModel(cd))
            }

        }
        refreshChannelList()

        if (paginator.hasNextPage()) {
            paginator.requestNextPage(object : CallbackListener<Paginator<ChannelDescriptor>>() {
                override fun onSuccess(channelDescriptorPaginator: Paginator<ChannelDescriptor>) {
                    getChannelsPage(channelDescriptorPaginator)
                }
            })
        } else {
            // Get subscribed channels last - so their status will overwrite whatever we received
            // from public list. Ugly workaround for now.
            val chans = basicClient.chatClient?.channels?.subscribedChannels
            if (chans != null) {
                for (channel in chans) {
                    if (channel.status == Channel.ChannelStatus.JOINED || channel.status == Channel.ChannelStatus.INVITED) {
                        channels.put(channel.sid, ChannelModel(channel))
                    }
                }
            }
            //disable shimmer loading
            refreshChannelList()
            channel_list.visibility = View.VISIBLE
            shimmer_view_container.visibility = View.GONE

        }
    }

    //Deeplink Channel open start

    fun checkIfChannelExists(user1: String, user2: String, isNewChannel: Boolean) {
        WeDialog.loading(this)

        basicClient = TwilioApplication.instance.basicClient

        basicClient.chatClient?.channels?.getUserChannelsList(object : CallbackListener<Paginator<ChannelDescriptor>>() {
            override fun onSuccess(channelDescriptorPaginator: Paginator<ChannelDescriptor>) {
                if (channelDescriptorPaginator.items.any { channels -> channels.uniqueName == "$user1-$user2" || "$user2-$user1" == channels.uniqueName }) {
                    channelDescriptorPaginator.items.forEach {

                        if (it.uniqueName == "$user1-$user2" || "$user2-$user1" == it.uniqueName) {
                            if (isNewChannel) {
                                openChannel(ChannelModel(it), user1, user2, true)
                            } else openChannel(ChannelModel(it), user1, user2, false)

                        }
                    }

                } else {
                    getChannel(user1, user2)
                }
            }
        })

    }

    fun getChannel(user1: String, user2: String) {
        basicClient = TwilioApplication.instance.basicClient

        basicClient.chatClient?.channels?.getChannel("$user1-$user2", object : CallbackListener<Channel>() {
            override fun onSuccess(channel: Channel) {
                channel!!.join(null)
                openChannel(ChannelModel(channel), user1, user2, false)

            }

            override fun onError(errorInfo: ErrorInfo?) {

                basicClient.chatClient?.channels?.getChannel("$user2-$user1", object : CallbackListener<Channel>() {
                    override fun onSuccess(channel: Channel) {
                        channel!!.join(null)
                        openChannel(ChannelModel(channel), user1, user2, false)

                    }

                    override fun onError(errorInfo: ErrorInfo?) {
                        createChannel(user1, user2)
                    }
                })
            }
        })
    }

    fun createChannel(user1: String, user2: String) {
        basicClient = TwilioApplication.instance.basicClient

        val builder = basicClient.chatClient?.channels?.channelBuilder()

        Log.e("creating channel", "$user1-$user2")

        builder?.withFriendlyName("")
                ?.withUniqueName("$user1-$user2")
                ?.withType(Channel.ChannelType.PRIVATE)
                ?.build(object : CallbackListener<Channel>() {
                    override fun onSuccess(newChannel: Channel) {

                        checkIfChannelExists(user1, user2, true)

                    }

                    override fun onError(errorInfo: ErrorInfo?) {
                        Log.e("Channel Creation error", "" + errorInfo)
                    }
                })
    }


    fun openChannel(channel: ChannelModel, user1: String, user2: String, isNewChannel: Boolean) {
        WeDialog.dismiss()

        if (isNewChannel) {
            channel.getChannel(ChatCallbackListener<Channel>() {
                it!!.join(
                        ToastStatusListener("Successfully joined channel",
                                "Failed to join channel") {
                        })

                it!!.members.addByIdentity(user2, ToastStatusListener(
                        "Successful addByIdentity",
                        "Error adding member"))

                Log.e("user2", "$user2")

            })

        }


        Handler().postDelayed({
            channel.getChannel(ChatCallbackListener<Channel>() {
                it!!.join(
                        ToastStatusListener("Successfully joined channel",
                                "Failed to join channel") {
                        })
                startActivity<MessageActivity>(
                        Constants.EXTRA_CHANNEL to it,
                        Constants.EXTRA_CHANNEL_SID to it.sid
                )
            })
        }, 0)

    }
    //Deeplink Channel open end

    // Initialize channels with channel list
    private fun getChannels() {
        channels.clear()

        basicClient.chatClient?.channels?.getPublicChannelsList(object : CallbackListener<Paginator<ChannelDescriptor>>() {
            override fun onSuccess(channelDescriptorPaginator: Paginator<ChannelDescriptor>) {
                getChannelsPage(channelDescriptorPaginator)
            }
        })

        basicClient.chatClient?.channels?.getUserChannelsList(object : CallbackListener<Paginator<ChannelDescriptor>>() {
            override fun onSuccess(channelDescriptorPaginator: Paginator<ChannelDescriptor>) {
                getChannelsPage(channelDescriptorPaginator)
            }
        })
    }

    private fun showIncomingInvite(channel: Channel) {
        alert(R.string.channel_invite_message, R.string.channel_invite) {
            customView {
                verticalLayout {
                    textView {
                        text = "You are invited to channel ${channel.friendlyName} |${channel.sid}|"
                        padding = dip(10)
                    }.lparams(width = matchParent)
                    positiveButton(R.string.join) {
                        channel.join(ToastStatusListener(
                                "Successfully joined channel",
                                "Failed to join channel") {
                            channels.put(channel.sid, ChannelModel(channel))
                            refreshChannelList()
                        })
                    }
                    negativeButton(R.string.decline) {
                        channel.declineInvitation(ToastStatusListener(
                                "Successfully declined channel invite",
                                "Failed to decline channel invite"))
                    }
                }
            }
        }.show()
    }

    private inner class CustomChannelComparator : Comparator<ChannelModel> {
        override fun compare(lhs: ChannelModel, rhs: ChannelModel): Int {
            var date1 = rhs.lastMessageDate ?: rhs.dateCreatedAsDate
            var date2 = lhs.lastMessageDate ?: lhs.dateCreatedAsDate
            return date1!!.compareTo(date2)
        }
    }

    //=============================================================
    // ChatClientListener
    //=============================================================

    override fun onChannelJoined(channel: Channel) {
        debug { "Received onChannelJoined callback for channel |${channel.friendlyName}|" }
        channels.put(channel.sid, ChannelModel(channel))
        refreshChannelList()
    }

    override fun onChannelAdded(channel: Channel) {
        debug { "Received onChannelAdded callback for channel |${channel.friendlyName}|" }
        channels.put(channel.sid, ChannelModel(channel))
        refreshChannelList()
    }

    override fun onChannelUpdated(channel: Channel, reason: Channel.UpdateReason) {
        debug { "Received onChannelUpdated callback for channel |${channel.friendlyName}| with reason ${reason}" }
        channels.put(channel.sid, ChannelModel(channel))
        var members = channel.members
        reason.name
        Log.e("reason", "" + reason.name)
        refreshChannelList()
    }

    override fun onChannelDeleted(channel: Channel) {
        debug { "Received onChannelDeleted callback for channel |${channel.friendlyName}|" }
        channels.remove(channel.sid)
        refreshChannelList()
    }

    override fun onChannelInvited(channel: Channel) {
        channels.put(channel.sid, ChannelModel(channel))
        refreshChannelList()
        // showIncomingInvite(channel)
        channel!!.join(
                ToastStatusListener("Successfully joined channel",
                        "Failed to join channel") {
                })

        refreshChannelList()
    }

    override fun onChannelSynchronizationChange(channel: Channel) {
        error { "Received onChannelSynchronizationChange callback for channel |${channel.friendlyName}| with new status ${channel.status}" }
        refreshChannelList()
    }

    override fun onClientSynchronization(status: ChatClient.SynchronizationStatus) {
        error { "Received onClientSynchronization callback ${status}" }
    }

    override fun onUserUpdated(user: User, reason: User.UpdateReason) {
        error { "Received onUserUpdated callback for ${reason}" }
    }

    override fun onUserSubscribed(user: User) {
        error { "Received onUserSubscribed callback" }
    }

    override fun onUserUnsubscribed(user: User) {
        error { "Received onUserUnsubscribed callback" }
    }

    override fun onNewMessageNotification(channelSid: String?, messageSid: String?, messageIndex: Long) {
        //TwilioApplication.instance.showToast("Received onNewMessage push notification")
        /*val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val importance = NotificationManager.IMPORTANCE_HIGH
        val notificationChannel = NotificationChannel("witty_chat", "Witty Chat", importance)

        notificationChannel.enableLights(true)
        notificationChannel.lightColor = Color.parseColor("#BD2381")
        notificationChannel.enableVibration(true)
        notificationChannel.vibrationPattern = longArrayOf(1000, 2000)
        notificationManager.createNotificationChannel(notificationChannel)

        val notification = NotificationCompat.Builder(this, "witty_chat")
                .setSmallIcon(R.drawable.ic_witty_chat_round)
                .setContentTitle("Witty Chat")
                .setContentText("New Message!")
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setChannelId("witty_chat")
                .setContentIntent(pendingIntent)
                .setColor(Color.rgb(214, 10, 37))
                .build()
        notificationManager.notify(0, notification)*/
    }

    override fun onAddedToChannelNotification(channelSid: String?) {
        TwilioApplication.instance.showToast("Received onAddedToChannel push notification")
    }

    override fun onInvitedToChannelNotification(channelSid: String?) {
        TwilioApplication.instance.showToast("Received onInvitedToChannel push notification")
    }

    override fun onRemovedFromChannelNotification(channelSid: String?) {
        TwilioApplication.instance.showToast("Received onRemovedFromChannel push notification")
    }

    override fun onNotificationSubscribed() {
        TwilioApplication.instance.showToast("Subscribed to push notifications")
    }

    override fun onNotificationFailed(errorInfo: ErrorInfo) {
        TwilioApplication.instance.showError("Failed to subscribe to push notifications", errorInfo)
    }

    override fun onError(errorInfo: ErrorInfo) {
        TwilioApplication.instance.showError("Received error", errorInfo)
    }

    override fun onConnectionStateChange(connectionState: ChatClient.ConnectionState) {
        //Toastie.info(this, "Status $connectionState", Toast.LENGTH_SHORT).show();
    }

    override fun onTokenExpired() {
        basicClient.onTokenExpired()
    }

    override fun onTokenAboutToExpire() {
        basicClient.onTokenAboutToExpire()
    }

    companion object {
        private val CHANNEL_OPTIONS = arrayOf("Join")
        private val JOIN = 0
    }

    override fun onBackPressed() {
        val a = Intent(Intent.ACTION_MAIN)
        a.addCategory(Intent.CATEGORY_HOME)
        a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(a)
    }

}

class CreateNewSheetFragment(roomDatabase: RoomDatabase, basicClient: BasicChatClient, context: Context) : SuperBottomSheetFragment() {
    var db = roomDatabase
    var bc = basicClient
    var ctxt = context


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var v = inflater.inflate(R.layout.bottom_create_new_panel_layout, container, false)
        v.title.text = "Create New"
        v.chat_text.text = "Chat"
        v.group_text.text = "Group"
        v.chat_btn.setOnClickListener {
            startActivity(Intent(ctxt, NewMessageActivity::class.java))
            this.dismiss()
        }
        v.group_btn.setOnClickListener {
            startActivity(Intent(ctxt, NewGroupActivity::class.java))
            this.dismiss()
        }
        super.onCreateView(inflater, container, savedInstanceState)
        return v
    }

    override fun getStatusBarColor() = Color.RED
    override fun getCornerRadius() = context!!.resources.getDimension(R.dimen.demo_sheet_rounded_corner)
    override fun getExpandedHeight() = -1
}
