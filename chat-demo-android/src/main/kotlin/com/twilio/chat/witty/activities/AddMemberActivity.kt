package com.twilio.chat.witty.activities

import ChatCallbackListener
import ToastStatusListener
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import com.cysion.wedialog.WeDialog
import com.mrntlu.toastie.Toastie
import com.twilio.chat.*
import com.twilio.chat.witty.*
import com.twilio.chat.witty.R
import com.twilio.chat.witty.api.RetrofitClient
import com.twilio.chat.witty.models.ContactsDetailsModel
import com.twilio.chat.witty.models.ContactsModel
import com.twilio.chat.witty.views.AddedToGroupViewHolder
import com.twilio.chat.witty.views.NewMessageViewHolder
import eu.inloop.simplerecycleradapter.ItemClickListener
import eu.inloop.simplerecycleradapter.SettableViewHolder
import eu.inloop.simplerecycleradapter.SimpleRecyclerAdapter
import kotlinx.android.synthetic.main.activity_add_member_to_channel.*
import kotlinx.android.synthetic.main.activity_new_group.*
import kotlinx.android.synthetic.main.activity_new_message.*
import kotlinx.android.synthetic.main.activity_new_message.backBtn
import org.jetbrains.anko.debug
import org.jetbrains.anko.startActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


class AddMemberActivity : AppCompatActivity(), ItemClickListener<ContactsDetailsModel> {

    private lateinit var adapter: SimpleRecyclerAdapter<ContactsDetailsModel>
    private lateinit var adapter2: SimpleRecyclerAdapter<ContactsDetailsModel>
    var addMemberList: ArrayList<ContactsDetailsModel> = ArrayList()
    private lateinit var basicClient: BasicChatClient
    private var channel: Channel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_member_to_channel)

        basicClient = TwilioApplication.instance.basicClient

        if (intent != null) {
            channel = intent.getParcelableExtra<Channel>(Constants.EXTRA_CHANNEL)
        }

        addMemberBtn.setOnClickListener {
            if(addMemberList.isNotEmpty()){
                WeDialog.loading(this@AddMemberActivity)
                addMember()
            }else  Toastie.error(this@AddMemberActivity, "Select a member to add", Toast.LENGTH_SHORT).show()

        }

        backBtn.setOnClickListener {

            super.onBackPressed()
        }

        adapter = SimpleRecyclerAdapter(this,
                object : SimpleRecyclerAdapter.CreateViewHolder<ContactsDetailsModel>() {
                    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettableViewHolder<ContactsDetailsModel> {
                        return NewMessageViewHolder(this@AddMemberActivity, parent)
                    }
                })


        adapter2 = SimpleRecyclerAdapter(
                { item: ContactsDetailsModel, _, view ->
                    addMemberList.remove(item)
                    adapter2.removeItem(item)
                    adapter2.notifyDataSetChanged()
                    if (addMemberList.isEmpty()) {
                        added_member_list_layout.visibility = View.GONE
                        adapter2.notifyDataSetChanged()
                    }

                },
                object : SimpleRecyclerAdapter.CreateViewHolder<ContactsDetailsModel>() {
                    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettableViewHolder<ContactsDetailsModel> {
                        return AddedToGroupViewHolder(this@AddMemberActivity, parent)
                    }
                })


        getEmployees(Globals.currentEmployeeDetails.org_id)

        added_member_list.adapter = adapter2

        added_member_list.layoutManager = LinearLayoutManager(this).apply {
            orientation = LinearLayoutManager.HORIZONTAL
        }

        AddMemberMessageSearch.doAfterTextChanged {
            filterContactsList()
        }
    }

    fun getEmployees(org_id: Int) {

        WeDialog.loading(this@AddMemberActivity)
        RetrofitClient.getContacts(org_id, object : Callback<ContactsModel> {

            override fun onResponse(call: Call<ContactsModel>, response: Response<ContactsModel>) {

                val responseModel = response.body()

                when {
                    response.isSuccessful && responseModel != null -> {
                        WeDialog.dismiss()

                        Globals.contactList = responseModel.record

                        initData()

                        add_member_contact_list.adapter = adapter

                        add_member_contact_list.layoutManager = LinearLayoutManager(this@AddMemberActivity).apply {
                            orientation = LinearLayoutManager.VERTICAL
                        }

                    }
                    else -> {
                        Toastie.warning(this@AddMemberActivity, "Error when getting list of employees", Toast.LENGTH_SHORT).show()
                        WeDialog.dismiss()
                    }
                }
            }

            override fun onFailure(call: Call<ContactsModel>, t: Throwable) {

                Toastie.error(this@AddMemberActivity, "Cannot Connect To Server", Toast.LENGTH_SHORT).show()
                Log.e("Employee List Error", "" + t.message)
                WeDialog.dismiss()
            }
        })
    }
    private fun filterContactsList() {

        var filteredList: ArrayList<ContactsDetailsModel> = ArrayList()
        filteredList.clear()
        Globals.contactList.forEach {
            if (it.middle_initial != null && it.last_name != null) {

                if (it.first_name!!.toLowerCase()!!.contains(AddMemberMessageSearch.text.toString().toLowerCase()) || it.middle_initial!!.toLowerCase()!!.contains(AddMemberMessageSearch.text.toString().toLowerCase()) || it.last_name!!.toLowerCase()!!.contains(AddMemberMessageSearch.text.toString().toLowerCase())) {
                    filteredList.add(it)
                }
            } else if (it.middle_initial == null && it.last_name != null) {

                if (it.first_name!!.toLowerCase()!!.contains(AddMemberMessageSearch.text.toString().toLowerCase()) || it.last_name!!.toLowerCase()!!.contains(AddMemberMessageSearch.text.toString().toLowerCase())) {
                    filteredList.add(it)
                }

            } else {
                if (it.first_name!!.toLowerCase()!!.contains(AddMemberMessageSearch.text.toString().toLowerCase())) {
                    filteredList.add(it)
                }
            }
            adapter.clear()
            adapter.addItems(filteredList)
            adapter.notifyDataSetChanged()
        }
    }

    private fun initData() {

        Collections.sort(Globals.contactList, Comparator { v1, v2 -> v1.first_name!!.compareTo(v2.first_name.toString()) })

        Globals.contactList.forEach {
            if (it.email != Globals.currentEmployeeDetails.email) {
                adapter.addItem(it)
            }
        }
        adapter.notifyDataSetChanged()
    }

    override fun onItemClick(item: ContactsDetailsModel, viewHolder: SettableViewHolder<ContactsDetailsModel>, view: View) {

        if (addMemberList.isEmpty()) {
            addMemberList.add(item)
            adapter2.addItem(item)
            adapter2.notifyDataSetChanged()
            added_member_list_layout.visibility = View.VISIBLE
        } else if (addMemberList.any { it.email != item.email }) {
            addMemberList.add(item)
            adapter2.addItem(item)
            adapter2.notifyDataSetChanged()

        }

    }

    fun addMember() {

        addMemberList.forEach {
            channel!!.members.addByIdentity(it.email, null)
        }

        WeDialog.dismiss()
        finish()
    }


}
