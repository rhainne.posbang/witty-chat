package com.twilio.chat.witty.models

data class UserOrgDetailsModel(val org_id: Int, val org_name:String)
