package com.twilio.chat.witty.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.room.Room
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.twilio.chat.witty.Globals
import com.twilio.chat.witty.R
import com.twilio.chat.witty.activities.ChannelActivity
import com.twilio.chat.witty.activities.LoginActivity
import com.twilio.chat.witty.database.RoomDatabase
import com.twilio.chat.witty.models.ContactsDetailsModel
import java.util.*

class NotifyFirebaseMessagingService : FirebaseMessagingService() {
    /**
     * Called when message is received.
     *
     * @param message The remote message, containing from, and message data as key/value pairs.
     */
    private lateinit var roomDatabase: RoomDatabase

    override fun onMessageReceived(message: RemoteMessage) {
        roomDatabase = Room.databaseBuilder(applicationContext, RoomDatabase::class.java, "maindb").allowMainThreadQueries().build()
        /*
         * The Notify service adds the message body to the remote message data so that we can
         * show a simple notification.
         */
        val from = message.from
        val data = message.data
        val title = data[NOTIFY_TITLE_DATA_KEY]
        val body = data[NOTIFY_BODY_DATA_KEY]
        Log.e(TAG, "Title: $title")
        Log.e(TAG, "From: $from")
        Log.e(TAG, "Body: $body")


        val bodyMessage: List<String> = body!!.split(":")

        if (bodyMessage[0] == "NewMessage"){
            if(bodyMessage[1].contains(".com-")){
                if (bodyMessage[3].isEmpty()){
                    sendNotification(getEmployeeName(bodyMessage[2]), "Sent a File or Image",bodyMessage[2])
                }else sendNotification(getEmployeeName(bodyMessage[2]), bodyMessage[3],bodyMessage[2])
            }else sendNotification(bodyMessage[1], getEmployeeName(bodyMessage[2]) + ": " + bodyMessage[3],bodyMessage[2])
        } else if (bodyMessage[0] == "AddedToChannel"){
            sendNotification("Added to Channel", "${getEmployeeName(bodyMessage[2])} Added you to ${bodyMessage[1]}",bodyMessage[2])
        }else if (bodyMessage[0] == "InvitedToChannel"){
            sendNotification("Invited To Channel", "${getEmployeeName(bodyMessage[2])} Invited you to ${bodyMessage[1]}",bodyMessage[2])
        }else if (bodyMessage[0] == "RemovedFromChannel"){
            sendNotification("Removed From Channel", "${getEmployeeName(bodyMessage[2])} Removed you from ${bodyMessage[1]}",bodyMessage[2])
        }
    }


    fun getEmployeeName(email: String): String {
        var allEmployees: List<ContactsDetailsModel> = roomDatabase.dbDao().getAllEmployees()
        var employeName = ""

        if(allEmployees.any { it.email==email }){
            allEmployees.forEach {
                if (it.email == email) {
                    employeName = "${it.first_name} ${it.last_name}"
                }
            }
        }else{
            employeName = email
        }

        return employeName
    }


    /**
     * Create and show a simple notification containing the GCM message.
     *
     * @param message GCM message received.
     */
    private fun sendNotification(title: String?, message: String?,sender: String) {
        var intent :Intent = Intent(this, ChannelActivity::class.java)
        if (Globals.contactList.isEmpty()){
            intent = Intent(this, LoginActivity::class.java)
        }
        Globals.NotificationUser = sender

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.witty_chat_logo)
                .setContentTitle(title)
                .setContentText(message)
                .setColor(Color.parseColor("#4EA6FF"))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setSound(defaultSoundUri)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        //if this is an Android Oreo device, set the notification channel
        val CHANNEL_ID = "notify_channel"
        val CHANNEL_NAME = "Notify Channel"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(CHANNEL_ID,
                    "CHANNEL_NAME", NotificationManager.IMPORTANCE_HIGH)
            notificationBuilder.setChannelId(CHANNEL_ID)
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify((Date().time / 1000L % Int.MAX_VALUE).toInt(), notificationBuilder.build())
    }

    companion object {
        private const val TAG = "NotifyFCMService"

        /*
     * The Twilio Notify message data keys are as follows:
     *  "twi_body"   // The body of the message
     *  "twi_title"  // The title of the message
     *
     * You can find a more detailed description of all supported fields here:
     * https://www.twilio.com/docs/api/notifications/rest/notifications#generic-payload-parameters
     */
        private const val NOTIFY_TITLE_DATA_KEY = "twi_title"
        private const val NOTIFY_BODY_DATA_KEY = "twi_body"
    }
}