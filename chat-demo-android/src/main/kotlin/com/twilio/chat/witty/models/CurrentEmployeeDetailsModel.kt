package com.twilio.chat.witty.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "current_employee")
data class CurrentEmployeeDetailsModel(@PrimaryKey val id: Int, val org_id: Int,  val user_id: Int, val first_name:  String?, val last_name:
String?, val middle_initial:  String?, val organization_name : String?, val email : String?, val image_path : String?, val current_employee_id : Int?, val organization_slug : String)
