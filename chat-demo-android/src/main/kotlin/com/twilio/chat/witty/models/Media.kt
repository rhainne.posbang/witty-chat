package com.twilio.chat.witty.models

import java.io.InputStream

data class Media(val name: String, val type: String, val stream: InputStream)
