package com.twilio.chat.witty.views

import android.content.Context
import com.twilio.chat.witty.R
import android.view.ViewGroup
import android.widget.TextView
import com.twilio.chat.witty.models.BoardsDetailsModel
import kotterknife.bindView
import eu.inloop.simplerecycleradapter.SettableViewHolder

class BoardViewHolder : SettableViewHolder<BoardsDetailsModel> {
    private val boardName: TextView by bindView(R.id.board_name)

    constructor(context: Context, parent: ViewGroup)
        : super(context, R.layout.boards_item_layout, parent)
    {}

    override fun setData(data: BoardsDetailsModel) {
        boardName.text = data.title
    }
}
