package com.twilio.chat.witty.activities

import ChatCallbackListener
import ChatStatusListener
import ToastStatusListener
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.andrefrsousa.superbottomsheet.SuperBottomSheetFragment
import com.cysion.wedialog.WeDialog
import com.mrntlu.toastie.Toastie
import com.noowenz.customdatetimepicker.CustomDateTimePicker
import com.pramonow.endlessrecyclerview.EndlessScrollCallback
import com.twilio.chat.*
import com.twilio.chat.Channel.ChannelType
import com.twilio.chat.witty.Constants
import com.twilio.chat.witty.Globals
import com.twilio.chat.witty.R
import com.twilio.chat.witty.TwilioApplication
import com.twilio.chat.witty.api.RetrofitClient
import com.twilio.chat.witty.models.*
import com.twilio.chat.witty.services.MediaService
import com.twilio.chat.witty.views.MemberViewHolder
import com.twilio.chat.witty.views.MessageViewHolder
import eu.inloop.simplerecycleradapter.ItemClickListener
import eu.inloop.simplerecycleradapter.ItemLongClickListener
import eu.inloop.simplerecycleradapter.SettableViewHolder
import eu.inloop.simplerecycleradapter.SimpleRecyclerAdapter
import kotlinx.android.synthetic.main.activity_message.*
import kotlinx.android.synthetic.main.bottom_select_accounts_panel_layout.view.*
import kotlinx.android.synthetic.main.bottom_select_column_panel_layout.view.*
import kotlinx.android.synthetic.main.bottom_select_org_panel_layout.view.confirmBtn
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.ankoView
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

// RecyclerView Anko
fun ViewManager.recyclerView() = recyclerView(theme = 0) {}

inline fun ViewManager.recyclerView(init: RecyclerView.() -> Unit): RecyclerView {
    return ankoView({ RecyclerView(it) }, theme = 0, init = init)
}

fun ViewManager.recyclerView(theme: Int = 0) = recyclerView(theme) {}

inline fun ViewManager.recyclerView(theme: Int = 0, init: RecyclerView.() -> Unit): RecyclerView {
    return ankoView({ RecyclerView(it) }, theme, init)
}
// End RecyclerView Anko

class MessageActivity : AppCompatActivity(), ChannelListener, AnkoLogger {
    private lateinit var adapter: SimpleRecyclerAdapter<MessageListModel>
    private var channel: Channel? = null

    private val messageItemList = ArrayList<MessageItem>()
    private lateinit var identity: String

    var OrgURl: String = "https://${Globals.currentEmployeeDetails?.organization_slug}.wittymanager.com/"
            ?: ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message)
        if (intent != null) {
            channel = intent.getParcelableExtra<Channel>(Constants.EXTRA_CHANNEL)
            if (channel != null) {
                setupListView(channel!!)
            }
        }
        createUI()


        Log.e("new task url", "${OrgURl}board/board.project_id/board.slug#cardID")
    }

    override fun onDestroy() {
        channel?.removeListener(this@MessageActivity);
        super.onDestroy();
    }

    override fun onResume() {
        super.onResume()
        /*  if (intent != null) {
              channel = intent.getParcelableExtra<Channel>(Constants.EXTRA_CHANNEL)
              if (channel != null) {
                  setupListView(channel!!)
              }
          }*/
    }

    fun getEmployeeName(email: String): String {
        var employeName = ""

        if (Globals.contactList.any { it.email == email }) {
            Globals.contactList.forEach {
                if (it.email == email) {
                    employeName = "${it.first_name} ${it.last_name}"
                }
            }
        } else {
            employeName = email
        }

        return employeName
    }

    override fun onBackPressed() {
        finish()
    }

    private fun createUI() {
        backBtn.setOnClickListener {
            finish()
        }

        messageSettingsBtn.setOnClickListener {
            showChannelSettingsDialog()
        }

        if (!channel!!.uniqueName.contains("-")) {
            reachabilityText.visibility = View.GONE
        } else {
            reachabilityText.visibility = View.VISIBLE
        }


        if (intent != null) {
            val basicClient = TwilioApplication.instance.basicClient
            identity = basicClient.chatClient!!.myIdentity

            val channelSid = intent.getStringExtra(Constants.EXTRA_CHANNEL_SID)
            val channelsObject = basicClient.chatClient!!.channels

            channelsObject.getChannel(channelSid, ChatCallbackListener<Channel>() {
                channel = it
                channel!!.addListener(this@MessageActivity)
                if (channel!!.type == ChannelType.PRIVATE && channel!!.uniqueName.contains("-")) {
                    val channelName: List<String> = channel!!.uniqueName.split("-")

                    if (channelName[0] != Globals.currentEmployeeDetails.email) {
                        personName.text = getEmployeeName(channelName[0])
                    } else if (channelName[1] != Globals.currentEmployeeDetails.email) {
                        personName.text = getEmployeeName(channelName[1])
                    }

                } else {
                    personName.text = channel!!.friendlyName
                }

                setupListView(channel!!)

//                    message_list_view.transcriptMode = ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL
//                    message_list_view.isStackFromBottom = true
//                    adapter.registerDataSetObserver(object : DataSetObserver() {
//                        override fun onChanged() {
//                            super.onChanged()
//                            message_list_view.setSelection(adapter.count - 1)
//                        }
//                    })
                setupInput()
            })


        }
        updateLastConsumedMessage()

        checkOnlineStatus()
    }

    private fun checkOnlineStatus() {

        val basicClient = TwilioApplication.instance.basicClient ?: null
        val channelSid = channel!!.sid ?: null
        val baseclient = basicClient?.chatClient!!.channels ?: null
        if (baseclient != null) {
            baseclient?.getChannel(channelSid, ChatCallbackListener<Channel>() {
                if (channel!!.type == ChannelType.PRIVATE) {
                    val channelName: List<String> = channel!!.uniqueName.split("-")

                    if (channelName[0] != Globals.currentEmployeeDetails.email) {
                        var member: Member? = it.members.getMember(channelName[0])
                        if (member != null) {
                            fillUserReachability(member)
                        }

                    } else if (channelName[1] != Globals.currentEmployeeDetails.email) {
                        var member: Member? = it.members.getMember(channelName[1])
                        if (member != null) {
                            fillUserReachability(member)
                        }
                    }

                }
            })
        }
    }

    private fun updateLastConsumedMessage() {
        if (Globals.currentEmployeeDetails.email != null) {
            var currentMember: Member? = channel!!.members.getMember(Globals.currentEmployeeDetails.email)
            if (channel!!.members != null && currentMember != null && channel!!.lastMessageIndex != null) {
                currentMember!!.channel!!.messages.setLastConsumedMessageIndexWithResult(channel!!.lastMessageIndex,
                        object : CallbackListener<Long>() {
                            override fun onSuccess(value: Long?) {
                                Log.e("MessageActivity", "setLastConsumedMessageIndexWithResult callback " + value.toString())
                            }
                        })
            }
        }

    }

    private fun advanceLastConsumedMessage() {
        var currentMember: Member? = channel!!.members.getMember(Globals.currentEmployeeDetails.email)
        if (channel!!.members != null && currentMember != null && channel!!.lastMessageIndex != null) {
            currentMember!!.channel!!.messages.setLastConsumedMessageIndexWithResult(channel!!.lastMessageIndex + 1,
                    object : CallbackListener<Long>() {
                        override fun onSuccess(value: Long?) {
                            Log.e("MessageActivity", "setLastConsumedMessageIndexWithResult callback " + value.toString())
                        }
                    })
        }
    }

    private fun fillUserReachability(member: Member) {
        if (!TwilioApplication.instance.basicClient.chatClient?.isReachabilityEnabled!!) {
            reachabilityMessageTitleBadge.setImageResource(R.drawable.reachability_disabled)
        } else {
            member.getAndSubscribeUser(object : CallbackListener<User>() {
                override fun onSuccess(user: User) {
                    if (user.isOnline) {
                        reachabilityText.text = "Active"
                        reachabilityMessageTitleBadge.setImageResource(R.drawable.reachability_online)
                    } else {
                        reachabilityText.text = "Offline"
                        reachabilityMessageTitleBadge.setImageResource(R.drawable.reachability_notifiable)
                    }
                }
            })
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> showChannelSettingsDialog()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showChannelSettingsDialog() {

        if (channel!!.uniqueName.contains(".com-")) {
            selector("Select an option", listOf("Change Name", "Destroy Channel", "Show Members", "Mute Notifications", "Unmute Notifications", "Cancel")) { selector, which ->
                when (which) {
                    0 -> showChangeNameDialog()
                    1 -> channel!!.destroy(ToastStatusListener(
                            "Successfully destroyed channel", "Error destroying channel") {
                        finish()
                    })
                    2 -> listMemberDialog()
                    3 -> channel!!.setNotificationLevel(Channel.NotificationLevel.MUTED, ToastStatusListener(
                            "Successfully disabled notifications", "Error disabling notifications") {
                    })
                    4 -> channel!!.setNotificationLevel(Channel.NotificationLevel.DEFAULT, ToastStatusListener(
                            "Successfully enabled notifications", "Error enabling notifications") {
                    })
                    5 -> selector.dismiss()

                }
            }

        } else {
            selector("Select an option", EDIT_OPTIONS_NEW) { selector, which ->
                when (which) {
                    0 -> showChangeNameDialog()
                    1 -> listMemberDialog()
                    2 -> newAddMemberDialog()
                    3 -> showRemoveMemberDialog()
                    4 -> channel!!.leave(ToastStatusListener(
                            "Successfully left channel", "Error leaving channel") {
                        finish()
                    })
                    5 -> channel!!.destroy(ToastStatusListener(
                            "Successfully destroyed channel", "Error destroying channel") {
                        finish()
                    })
                    6 -> channel!!.setNotificationLevel(Channel.NotificationLevel.MUTED, ToastStatusListener(
                            "Successfully disabled notifications", "Error disabling notifications") {
                    })
                    7 -> channel!!.setNotificationLevel(Channel.NotificationLevel.DEFAULT, ToastStatusListener(
                            "Successfully enabled notifications", "Error enabling notifications") {
                    })
                    8 -> selector.dismiss()
                }
            }

        }


        /*  selector("Select an option", EDIT_OPTIONS) { _, which ->
              when (which) {
                  NAME_CHANGE -> showChangeNameDialog()
                  TOPIC_CHANGE -> showChangeTopicDialog()
                  LIST_MEMBERS -> {
                      val users = TwilioApplication.instance.basicClient.chatClient!!.users
                      // Members.getMembersList() way
                      val members = channel!!.members.membersList
                      val name = StringBuffer()
                      for (i in members.indices) {
                          name.append(members[i].identity)
                          if (i + 1 < members.size) {
                              name.append(", ")
                          }
                          members[i].getUserDescriptor(ChatCallbackListener<UserDescriptor>() {
                              debug { "Got user descriptor from member: ${it.identity}" }
                          })
                          members[i].getAndSubscribeUser(ChatCallbackListener<User>() {
                              debug { "Got subscribed user from member: ${it.identity}" }
                          })
                      }
                      TwilioApplication.instance.showToast(name.toString(), Toast.LENGTH_LONG)
                      // Users.getSubscribedUsers() everybody we subscribed to at the moment
                      val userList = users.subscribedUsers
                      val name2 = StringBuffer()
                      for (i in userList.indices) {
                          name2.append(userList[i].identity)
                          if (i + 1 < userList.size) {
                              name2.append(", ")
                          }
                      }
                      //Log.e("Members", "${name2.toString()}")
                      TwilioApplication.instance.showToast("Subscribed users: ${name2.toString()}", Toast.LENGTH_LONG)

                      members.forEach {
                          Log.e("Members", "${it.identity}")
                      }
                      // Get user descriptor via identity
                      channel!!.members.membersList.forEach { member ->

                          users.getUserDescriptor(member.identity, ChatCallbackListener<UserDescriptor>() {
                              // Log.e("Members", "${it.identity}")
                             // TwilioApplication.instance.showToast("Random user descriptor: ${it.friendlyName}/${it.identity}", Toast.LENGTH_SHORT)
                          })
                      }

                      // Users.getChannelUserDescriptors() way - paginated
                      users.getChannelUserDescriptors(channel!!.sid,
                              object : CallbackListener<Paginator<UserDescriptor>>() {
                                  override fun onSuccess(userDescriptorPaginator: Paginator<UserDescriptor>) {
                                      getUsersPage(userDescriptorPaginator)
                                  }
                              })

                      // Channel.getMemberByIdentity() for finding the user in all channels
                      val members2 = TwilioApplication.instance.basicClient.chatClient!!.channels.getMembersByIdentity(channel!!.members.membersList[0].identity)
                      val name3 = StringBuffer()
                      for (i in members2.indices) {
                          name3.append(members2[i].identity + " in " + members2[i].channel.friendlyName)
                          if (i + 1 < members2.size) {
                              name3.append(", ")
                          }
                      }
                      //TwilioApplication.get().showToast("Random user in all channels: "+name3.toString(), Toast.LENGTH_LONG);
                  }
                  INVITE_MEMBER -> showInviteMemberDialog()
                  ADD_MEMBER -> showAddMemberDialog()
                  REMOVE_MEMBER -> showRemoveMemberDialog()
                  LEAVE -> channel!!.leave(ToastStatusListener(
                          "Successfully left channel", "Error leaving channel") {
                      finish()
                  })
                  CHANNEL_DESTROY -> channel!!.destroy(ToastStatusListener(
                          "Successfully destroyed channel", "Error destroying channel") {
                      finish()
                  })
                  CHANNEL_ATTRIBUTE -> try {
                      TwilioApplication.instance.showToast(channel!!.attributes.toString())
                  } catch (e: JSONException) {
                      TwilioApplication.instance.showToast("JSON exception in channel attributes")
                  }
                  SET_CHANNEL_UNIQUE_NAME -> showChangeUniqueNameDialog()
                  GET_CHANNEL_UNIQUE_NAME -> TwilioApplication.instance.showToast(channel!!.uniqueName)
                  GET_MESSAGE_BY_INDEX -> channel!!.messages.getMessageByIndex(channel!!.messages.lastConsumedMessageIndex!!, ChatCallbackListener<Message>() {
                      TwilioApplication.instance.showToast("SUCCESS GET MESSAGE BY IDX")
                      error { "MESSAGES ${it.messages.toString()}, CHANNEL ${it.channel.sid}" }
                  })
                  SET_ALL_CONSUMED -> channel!!.messages.setAllMessagesConsumedWithResult(ChatCallbackListener<Long>()
                  { unread -> TwilioApplication.instance.showToast("$unread messages still unread") })
                  SET_NONE_CONSUMED -> channel!!.messages.setNoMessagesConsumedWithResult(ChatCallbackListener<Long>()
                  { unread -> TwilioApplication.instance.showToast("$unread messages still unread") })
                  DISABLE_PUSHES -> channel!!.setNotificationLevel(Channel.NotificationLevel.MUTED, ToastStatusListener(
                          "Successfully disabled pushes", "Error disabling pushes") {
                      finish()
                  })
                  ENABLE_PUSHES -> channel!!.setNotificationLevel(Channel.NotificationLevel.DEFAULT, ToastStatusListener(
                          "Successfully enabled pushes", "Error enabling pushes") {
                      finish()
                  })
              }
          }*/
    }

    private fun getUsersPage(userDescriptorPaginator: Paginator<UserDescriptor>) {
        for (u in userDescriptorPaginator.items) {
            u.subscribe(ChatCallbackListener<User>() {
                debug { "${it.identity} is a subscribed user now" }
            })
        }
        if (userDescriptorPaginator.hasNextPage()) {
            userDescriptorPaginator.requestNextPage(ChatCallbackListener<Paginator<UserDescriptor>>() {
                getUsersPage(it)
            })
        }
    }

    private fun showChangeNameDialog() {
        alert(R.string.title_update_friendly_name) {
            customView {
                val friendly_name = editText { text.append(channel!!.friendlyName) }
                positiveButton(R.string.update) {
                    val friendlyName = friendly_name.text.toString()
                    debug { friendlyName }
                    channel!!.setFriendlyName(friendlyName, ToastStatusListener(
                            "Successfully changed name", "Error changing name"))
                    personName.text = friendlyName
                }
                negativeButton(R.string.cancel) {}
            }
        }.show()
    }

    private fun showChangeTopicDialog() {
        alert(R.string.title_update_topic) {
            customView {
                val topic = editText { text.append(channel!!.attributes.toString()) }
                positiveButton(R.string.change_topic) {
                    val topicText = topic.text.toString()
                    debug { topicText }

                    try { // @todo Get attributes to update
                        JSONObject().apply {
                            put("Topic", topicText)
                            channel!!.setAttributes(Attributes(this), ToastStatusListener(
                                    "Attributes were set successfullly.",
                                    "Setting attributes failed"))
                        }
                    } catch (ignored: JSONException) {
                        // whatever
                    }
                }
                negativeButton(R.string.cancel) {}
            }
        }.show()
    }

    private fun showInviteMemberDialog() {
        alert(R.string.title_invite_member) {
            customView {
                val member = editText { hint = "Enter user id" }
                positiveButton(R.string.invite_member) {
                    val memberName = member.text.toString()
                    debug { memberName }
                    channel!!.members.inviteByIdentity(memberName, ToastStatusListener(
                            "Invited user to channel",
                            "Error in inviteByIdentity"))
                }
                negativeButton(R.string.cancel) {}
            }
        }.show()
    }

    private fun showAddMemberDialog() {
        alert(R.string.title_add_member) {
            customView {
                val member = editText { hint = "Enter user id" }
                positiveButton(R.string.invite_member) {
                    val memberName = member.text.toString()
                    debug { memberName }
                    channel!!.members.addByIdentity(memberName, ToastStatusListener(
                            "Successful addByIdentity",
                            "Error adding member"))
                }
                negativeButton(R.string.cancel) {}
            }
        }.show()
    }

    private fun showRemoveMemberDialog() {
        alert("Remove members") {
            customView {
                verticalLayout {
                    val view = recyclerView {}.lparams(width = dip(250), height = matchParent)
                    negativeButton(R.string.cancel) {
                        it.dismiss()
                    }

                    view.adapter = SimpleRecyclerAdapter<Member>(
                            ItemClickListener { member: Member, _, view ->
                                channel!!.members.remove(member, ToastStatusListener(
                                        "Successful removeMember operation",
                                        "Error in removeMember operation"))
                            },
                            object : SimpleRecyclerAdapter.CreateViewHolder<Member>() {
                                override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettableViewHolder<Member> {
                                    return MemberViewHolder(this@MessageActivity, parent)
                                }
                            },
                            channel!!.members.membersList)

                    view.layoutManager = LinearLayoutManager(this@MessageActivity).apply {
                        orientation = LinearLayoutManager.VERTICAL
                    }
                }
            }
        }.show()
    }


    private fun listMemberDialog() {
        alert("All members") {
            customView {
                verticalLayout {
                    val view = recyclerView {}.lparams(width = dip(250), height = matchParent)
                    negativeButton("Close") {}

                    view.adapter = SimpleRecyclerAdapter<Member>(
                            null,
                            object : SimpleRecyclerAdapter.CreateViewHolder<Member>() {
                                override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettableViewHolder<Member> {
                                    return MemberViewHolder(this@MessageActivity, parent)
                                }
                            },
                            channel!!.members.membersList)

                    view.layoutManager = LinearLayoutManager(this@MessageActivity).apply {
                        orientation = LinearLayoutManager.VERTICAL
                    }
                }
            }
        }.show()
    }


    private fun newAddMemberDialog() {

        if (intent != null) {
            startActivity<AddMemberActivity>(
                    Constants.EXTRA_CHANNEL to channel,
                    Constants.EXTRA_CHANNEL_SID to channel!!.sid
            )
        }

        /*  alert("Add members") {
              customView {
                  verticalLayout {
                      val view = recyclerView {}.lparams(width = dip(250), height = matchParent)
                      negativeButton(R.string.cancel) {}

                      view.adapter = SimpleRecyclerAdapter<ContactsDetailsModel>(
                              ItemClickListener { item: ContactsDetailsModel, _, view ->
                                  channel!!.members.addByIdentity(item.email, ToastStatusListener(
                                          "Successful addByIdentity",
                                          "Error adding member"))
                              },
                              object : SimpleRecyclerAdapter.CreateViewHolder<ContactsDetailsModel>() {
                                  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettableViewHolder<ContactsDetailsModel> {
                                      return NewMemberViewHolder(this@MessageActivity, parent)
                                  }
                              },
                              Globals.contactList)

                      view.layoutManager = LinearLayoutManager(this@MessageActivity).apply {
                          orientation = LinearLayoutManager.VERTICAL
                      }
                  }
              }
          }.show()*/
    }


    private fun showUpdateMessageDialog(message: Message) {
        alert(R.string.title_update_message) {
            customView {
                val messageText = editText { text.append(message.messageBody) }
                positiveButton(R.string.update) {
                    val text = messageText.text.toString()
                    debug { text }
                    message.updateMessageBody(text, ToastStatusListener(
                            "Success updating message",
                            "Error updating message") {
                        // @todo only need to update one message body
                        loadAndShowMessages()
                    })
                }
                negativeButton(R.string.cancel) {}
            }
        }.show()
    }

    private fun showUpdateMessageAttributesDialog(message: Message) {
        alert(R.string.title_update_attributes) {
            customView {
                val messageAttrText = editText { text.append(message.attributes.toString()) }
                positiveButton(R.string.update) {
                    val text = messageAttrText.text.toString()
                    debug { text }
                    try {
                        JSONObject(text).apply {
                            message.setAttributes(Attributes(this), ToastStatusListener(
                                    "Success updating message attributes",
                                    "Error updating message attributes") {
                                // @todo only need to update one message
                                loadAndShowMessages()
                            })
                        }
                    } catch (e: JSONException) {
                        error { "Invalid JSON attributes entered, using old value" }
                    }
                }
                negativeButton(R.string.cancel) {}
            }
        }.show()
    }

    private fun showChangeUniqueNameDialog() {
        alert("Update channel unique name") {
            customView {
                val uniqueNameText = editText { text.append(channel!!.uniqueName) }
                positiveButton(R.string.update) {
                    val uniqueName = uniqueNameText.text.toString()
                    debug { uniqueName }
                    channel!!.setUniqueName(uniqueName, ChatStatusListener());
                }
                negativeButton(R.string.cancel) {}
            }
        }.show()
    }

    private fun loadAndShowMessages() {
        var messageList: ArrayList<MessageListModel> = ArrayList()

        //Set callback for loading more
        message_list_view.setEndlessScrollCallback(object : EndlessScrollCallback {
            //This function will load more list and add it inside the adapter
            override fun loadMore() {
                //Load more of you data here then update your adapter
                if (messageList.isNotEmpty()) {
                    messageProgressLoading.visibility = View.VISIBLE
                    message_list_view.blockLoading()
                    channel!!.messages?.getMessagesBefore(messageList[0].message.message.messageIndex, 2, ChatCallbackListener<List<Message>>() {
                        val members = channel!!.members
                        if (it.isNotEmpty()) {
                            for (i in it.indices) {
                                if (it[i].messageIndex < messageList[0].message.message.messageIndex) {
                                    if (channel!!.uniqueName.contains("-")) {
                                        messageList.add(0, MessageListModel(MessageItem(it[i], members, identity), false))
                                    } else {
                                        messageList.add(0, MessageListModel(MessageItem(it[i], members, identity), true))
                                    }
                                }
                                adapter.clear()
                                adapter.addItems(messageList.reversed())
                                adapter.notifyDataSetChanged()
                                messageProgressLoading.visibility = View.GONE
                                message_list_view.releaseBlock()
                            }
                        }
                    })
                }
            }
        })

        channel!!.messages?.getLastMessages(15, ChatCallbackListener<List<Message>>() {
            messageItemList.clear()
            val members = channel!!.members
            if (it.isNotEmpty()) {
                for (i in it.indices) {
                    Log.e("messageIndex", "${it[i].messageIndex} ${it[i].messageBody}")
                    messageItemList.add(MessageItem(it[i], members, identity))
                }
            }


            messageItemList.forEach { message ->

                if (channel!!.uniqueName.contains("-")) {
                    messageList.add(MessageListModel(message, false))
                } else {
                    messageList.add(MessageListModel(message, true))
                }
            }
            adapter.clear()
            adapter.addItems(messageList.reversed())
            message_list_view.layoutManager!!.scrollToPosition(0)
            adapter.notifyDataSetChanged()
        })

        message_list_view.addOnLayoutChangeListener { view, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
            // Wait till recycler_view will update itself and then scroll to the end.
            if (bottom < oldBottom) {
                message_list_view.post {
                    message_list_view.layoutManager!!.scrollToPosition(0)
                    Log.e("addOnLayoutChangeListener", "top($top) - oldTop($oldTop)")
                    Log.e("addOnLayoutChangeListener", "bottom($bottom) - oldbottom($oldBottom)")
                    Log.e("scroll up", "${message_list_view!!.canScrollVertically(-1)}")
                    Log.e("scroll down", "${message_list_view!!.canScrollVertically(1)}")
                    Log.e("child count", "${message_list_view.childCount}")

                    /* if (!message_list_view!!.canScrollVertically(1)){
                         message_list_view.layoutManager!!.scrollToPosition(0)
                     }*/
                }
            }

        }


        /* message_list_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
             override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                 super.onScrollStateChanged(recyclerView, newState)
                 if (!message_list_view.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
                     Log.e("-----","end")

                 }
             }

             override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                 if (dy < 0) {
                     // Recycle view scrolling up...
                     Log.e("scroll up", "${message_list_view!!.canScrollVertically(-1)}")
                 } else if (dy > 0) {
                     // Recycle view scrolling down...
                     Log.e("scroll down", "${message_list_view!!.canScrollVertically(1)}")
                 }
             }
         })
 */
    }

    private fun setupInput() {
        // Setup our input methods. Enter key on the keyboard or pushing the send button
        messageInput.apply {
            addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
                override fun afterTextChanged(s: Editable) {
                    if (channel != null) {
                        channel!!.typing()
                    }
                }
            })

            setOnEditorActionListener { _, actionId, keyEvent ->
                if (actionId == EditorInfo.IME_NULL && keyEvent.action == KeyEvent.ACTION_DOWN) {
                    sendMessage()
                }
                true
            }
        }

        addMediaBtn.setOnClickListener {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = "*/*"
            }
            this@MessageActivity.startActivityForResult(intent, FILE_REQUEST)
        }

        sendBtn.apply {
            setOnClickListener { sendMessage() }
        }

    }


    private inner class CustomMessageComparator : Comparator<Message> {
        override fun compare(lhs: Message?, rhs: Message?): Int {
            if (lhs == null) {
                return if (rhs == null) 0 else -1
            }
            if (rhs == null) {
                return 1
            }
            return lhs.dateCreated.compareTo(rhs.dateCreated)
        }
    }

    private fun setupListView(channel: Channel) {
        /*    message_list_view.viewTreeObserver.addOnScrollChangedListener {
                if (message_list_view.lastVisiblePosition >= 0 && message_list_view.lastVisiblePosition < adapter.itemCount) {
                    val item = adapter.getItem(message_list_view.lastVisiblePosition)
                    if (item != null && messagesObject != null)
                         channel.messages.advanceLastConsumedMessageIndex(
                                 item.message.messageIndex)
                }
            }
     */
        adapter = SimpleRecyclerAdapter<MessageListModel>(
                ItemClickListener { _: MessageListModel, viewHolder, _ ->
                    (viewHolder as MessageViewHolder).toggleDateVisibility()
                },
                object : SimpleRecyclerAdapter.CreateViewHolder<MessageListModel>() {
                    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettableViewHolder<MessageListModel> {
                        return MessageViewHolder(this@MessageActivity, parent);
                    }
                })
        adapter.setLongClickListener(
                ItemLongClickListener { message: MessageListModel, _, _ ->
                    selector("Select an option", MESSAGE_OPTIONS) { dialog, which ->
                        when (which) {
                            REMOVE -> {
                                dialog.cancel()
                                channel.messages.removeMessage(
                                        message.message.message, ToastStatusListener(
                                        "Successfully removed message. It should be GONE!!",
                                        "Error removing message") {
                                    messageItemList.remove(message.message)
                                })
                                adapter.notifyDataSetChanged()
                                adapter.clear()
                                loadAndShowMessages()
                            }
                            EDIT -> showUpdateMessageDialog(message.message.message)
                            GET_ATTRIBUTES -> {

                                copyTextToClipboard(message.message.message)
                                /*  try {
                                      TwilioApplication.instance.showToast(message.message.attributes.toString())
                                  } catch (e: JSONException) {
                                      TwilioApplication.instance.showToast("Error parsing message attributes")
                                  }*/
                            }
                            SET_ATTRIBUTES -> {
                                //showUpdateMessageAttributesDialog(message.message.message)

                                getBoards(message.message.message.messageBody.toString(), channel)

                            }
                            4 -> {
                                //showUpdateMessageAttributesDialog(message.message.message)

                                getAccounts(message.message.message.messageBody.toString(), channel)

                            }
                        }
                    }
                    true
                }
        )

        message_list_view.adapter = adapter
        message_list_view.itemAnimator!!.changeDuration = 0
        message_list_view.layoutManager = LinearLayoutManager(this).apply {
            orientation = LinearLayoutManager.VERTICAL
            this.stackFromEnd = true
            this.reverseLayout = true
        }

        loadAndShowMessages()
    }

    fun copyTextToClipboard(message: Message) {
        val textToCopy: String = message.messageBody.toString()
        val clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("text", textToCopy)
        clipboardManager.setPrimaryClip(clipData)
        Toast.makeText(this, "Text copied to clipboard", Toast.LENGTH_LONG).show()
    }

    fun createTask(command: String, args: ArgsModel) {

        WeDialog.loading(this)

        RetrofitClient.chatCommand(Globals.currentEmployeeDetails.org_id, Globals.currentEmployeeDetails.id, Globals.currentEmployeeDetails.id, command, "{\"board_id\": 26733, \"column_id\": 3610}",
                object : Callback<ChatCommandResponseModel> {

                    override fun onResponse(call: Call<ChatCommandResponseModel>, response: Response<ChatCommandResponseModel>) {

                        val responseModel = response.body()

                        when {
                            response.isSuccessful && responseModel != null -> {
                                WeDialog.dismiss()

                                Globals.chatCommandResponse = responseModel.command_response

                                Log.e("chat command response ", "" + responseModel.command_response.success)
                                var cardID: String = responseModel.command_response.card.id.toString()
                                        ?: ""
                                sendMessage("${OrgURl}board/352/card3#$cardID")

                            }
                            else -> {
                                //Toastie.warning(this@MessageActivity, "Error when getting list of employees", Toast.LENGTH_SHORT).show()
                                WeDialog.dismiss()
                            }
                        }
                    }

                    override fun onFailure(call: Call<ChatCommandResponseModel>, t: Throwable) {
                        Log.e("Create Task error", "" + t.message)
                        WeDialog.dismiss()
                    }
                })
    }

    fun sendMessage(text: String) {
        channel!!.messages.sendMessage(Message.options().withBody(text), ChatCallbackListener<Message>() {
            //TwilioApplication.instance.showToast("Successfully sent message");
            messageInput.setText("")
            adapter.notifyDataSetChanged()
        })
        advanceLastConsumedMessage()
        checkOnlineStatus()
    }

    private fun sendMessage() {
        val input = messageInput.text.toString()
        if (input != "") {
            sendMessage(input)
        }
    }

    /// Send media message
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == FILE_REQUEST && resultCode == Activity.RESULT_OK) {
            debug { "Uri: ${data?.data}" }

            startService<MediaService>(
                    MediaService.EXTRA_ACTION to MediaService.EXTRA_ACTION_UPLOAD,
                    MediaService.EXTRA_CHANNEL to channel as Parcelable,
                    MediaService.EXTRA_MEDIA_URI to data?.data.toString())
        }
    }

    override fun onMessageAdded(message: Message) {
        setupListView(channel!!)

        startService<MediaService>(
                MediaService.EXTRA_ACTION to MediaService.EXTRA_ACTION_DOWNLOAD,
                MediaService.EXTRA_CHANNEL to channel as Parcelable,
                MediaService.EXTRA_MESSAGE_INDEX to message.messageIndex)

    }

    override fun onMessageUpdated(message: Message?, reason: Message.UpdateReason) {
        checkOnlineStatus()
        if (message != null) {
            // TwilioApplication.instance.showToast("onMessageUpdated for ${message.sid}, changed because of ${reason}")
        } else {
            debug { "Received onMessageUpdated" }
        }

    }

    override fun onMessageDeleted(message: Message?) {
        checkOnlineStatus()
        if (message != null) {
            TwilioApplication.instance.showToast("onMessageDeleted for ${message.sid}")
        } else {
            debug { "Received onMessageDeleted." }
        }
    }

    override fun onMemberAdded(member: Member?) {
        if (member != null) {
            TwilioApplication.instance.showToast("${member.identity} joined")
        }
    }

    override fun onMemberUpdated(member: Member?, reason: Member.UpdateReason) {
        if (member != null) {
            //TwilioApplication.instance.showToast("${member.identity} changed because of ${reason}")
        }
    }

    override fun onMemberDeleted(member: Member?) {
        if (member != null) {
            TwilioApplication.instance.showToast("${member.identity} deleted")
        }
    }

    override fun onTypingStarted(channel: Channel?, member: Member?) {
        checkOnlineStatus()
        if (member != null) {
            typingIndicator.visibility = View.VISIBLE
            var employeeName = getEmployeeName(member.identity)
            val text = "$employeeName is typing ..."
            typingIndicator.text = text
            //typingIndicator.setTextColor(Color.RED)
            debug { text }
        }
    }

    override fun onTypingEnded(channel: Channel?, member: Member?) {
        checkOnlineStatus()
        if (member != null) {
            typingIndicator.visibility = View.GONE
            var employeeName = getEmployeeName(member.identity)
            typingIndicator.text = null
            debug { "$employeeName finished typing" }
        }
    }

    override fun onSynchronizationChanged(channel: Channel) {
        checkOnlineStatus()
        debug { "Received onSynchronizationChanged callback for ${channel.friendlyName}" }
    }

    data class MessageItem(val message: Message, val members: Members, internal var currentUser: String);

    companion object {
        //private val MESSAGE_OPTIONS = listOf("Remove", "Edit", "Get Attributes", "Edit Attributes")
        private val MESSAGE_OPTIONS = listOf("Remove", "Edit", "Copy", "Convert to Task", "Convert to Ticket")
        private val REMOVE = 0
        private val EDIT = 1
        private val GET_ATTRIBUTES = 2
        private val SET_ATTRIBUTES = 3

        private val EDIT_OPTIONS = listOf("Change Friendly Name", "Change Topic", "List Members", "Invite Member", "Add Member", "Remove Member", "Leave", "Destroy", "Get Attributes", "Change Unique Name", "Get Unique Name", "Get message index 0", "Set all consumed", "Set none consumed", "Disable Pushes", "Enable Pushes")
        private val EDIT_OPTIONS_NEW = listOf("Change Group Name", "List Members", "Add Member", "Remove Member", "Leave", "Destroy", "Mute Notifications", "Unmute Notifications", "Cancel")
        private val NAME_CHANGE = 0
        private val TOPIC_CHANGE = 1
        private val LIST_MEMBERS = 2
        private val INVITE_MEMBER = 3
        private val ADD_MEMBER = 4
        private val REMOVE_MEMBER = 5
        private val LEAVE = 6
        private val CHANNEL_DESTROY = 7
        private val CHANNEL_ATTRIBUTE = 8
        private val SET_CHANNEL_UNIQUE_NAME = 9
        private val GET_CHANNEL_UNIQUE_NAME = 10
        private val GET_MESSAGE_BY_INDEX = 11
        private val SET_ALL_CONSUMED = 12
        private val SET_NONE_CONSUMED = 13
        private val DISABLE_PUSHES = 14
        private val ENABLE_PUSHES = 15

        private val FILE_REQUEST = 1000;
    }

    private fun getBoards(taskText: String, currentChannel: Channel?) {

        WeDialog.loading(this)

        RetrofitClient.getBoards(Globals.currentEmployeeDetails.org_id,
                object : Callback<BoardsModel> {

                    override fun onResponse(call: Call<BoardsModel>, response: Response<BoardsModel>) {

                        val responseModel = response.body()

                        when {
                            response.isSuccessful && responseModel != null -> {
                                WeDialog.dismiss()
                                val sheet = CreateTaskSheetFragment(taskText, responseModel.boards, currentChannel)
                                sheet.show(supportFragmentManager, "Get Boards")

                            }
                            else -> {
                                WeDialog.dismiss()
                            }
                        }
                    }

                    override fun onFailure(call: Call<BoardsModel>, t: Throwable) {
                        Log.e("Get Boards error", "" + t.message)
                        WeDialog.dismiss()
                    }
                })
    }


    private fun getAccounts(ticketText: String, currentChannel: Channel?) {

        WeDialog.loading(this)

        RetrofitClient.getAccounts(Globals.currentEmployeeDetails.org_id,
                object : Callback<ArrayList<AccountsDetailsModel>> {

                    override fun onResponse(call: Call<ArrayList<AccountsDetailsModel>>, response: Response<ArrayList<AccountsDetailsModel>>) {

                        val responseModel = response.body()

                        when {
                            response.isSuccessful && responseModel != null -> {
                                WeDialog.dismiss()

                                val sheet = CreateTicketSheetFragment(ticketText, responseModel, this@MessageActivity, currentChannel)
                                sheet.show(supportFragmentManager, "Get Accounts")

                            }
                            else -> {
                                WeDialog.dismiss()
                            }
                        }
                    }

                    override fun onFailure(call: Call<ArrayList<AccountsDetailsModel>>, t: Throwable) {
                        Log.e("Get Accounts error", "" + t.message)
                        WeDialog.dismiss()
                    }
                })
    }

}

class CreateTaskSheetFragment(taskText: String?, boardList: ArrayList<BoardsDetailsModel>, channel: Channel?) : SuperBottomSheetFragment() {
    var tt = taskText
    var bl = boardList
    var cl = channel
    var OrgURl: String = "https://${Globals.currentEmployeeDetails?.organization_slug}.wittymanager.com/"
            ?: ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var v = inflater.inflate(R.layout.bottom_select_column_panel_layout, container, false)
        var selectedColumn: Int = 0
        v.newTasktitle.text = "Create New Task"
        v.boardLabel.text = "Select Board"
        v.columnLabel.text = "Select Column"

        v.selectBoardSpinner.setTitle("Select Board")
        v.selectBoardSpinner.setPositiveButton("OK")

        v.selectColumnSpinner.setTitle("Select Column")
        v.selectColumnSpinner.setPositiveButton("OK")

        val boardSpinnerList = ArrayList<String>()
        val columnSpinnerList = ArrayList<String>()

        //Board Spinner
        Collections.sort(bl, Comparator { v1, v2 -> v1.title!!.compareTo(v2.title.toString()) })

        bl.forEach {
            Log.e("is sprint", "${it.title} ${it.is_sprint_board}")
            if(it.is_sprint_board == 0){
                boardSpinnerList.add(it.title)
            }
        }

        val boardSpinnerAdapter = ArrayAdapter(
                v.context, android.R.layout.simple_spinner_item, boardSpinnerList
        )

        v.selectBoardSpinner.adapter = boardSpinnerAdapter


        //Column Spinner
        bl.forEach {
            if (it.title == v.selectBoardSpinner.selectedItem.toString()) {
                it.columns.forEach { columns ->
                    columnSpinnerList.add(columns.name)
                }
            }
        }


        val selectSpinnerAdapter = ArrayAdapter(
                v.context, android.R.layout.simple_spinner_item, columnSpinnerList
        )

        v.selectColumnSpinner.adapter = selectSpinnerAdapter


        v.selectBoardSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                columnSpinnerList.clear()
                bl.forEach {
                    if (it.title == v.selectBoardSpinner.selectedItem.toString()) {
                        it.columns.forEach { columns ->
                            columnSpinnerList.add(columns.name)
                        }
                    }
                }
                selectSpinnerAdapter.notifyDataSetChanged()
            }
        }

        v.createTaskBtn.setOnClickListener {
            bl.forEach {
                if (it.title == v.selectBoardSpinner.selectedItem.toString()) {
                    it.columns.forEach { columns ->
                        if (columns.name == v.selectColumnSpinner.selectedItem.toString()) {
                            createTask("\$" + "card " + tt, ArgsModel(columns.board_id, columns.id), it, this)
                        }
                    }
                }
            }
            this.dismiss()
        }

        super.onCreateView(inflater, container, savedInstanceState)
        return v
    }

    private fun createTask(command: String, args: ArgsModel, board: BoardsDetailsModel, bottomSheet: SuperBottomSheetFragment) {

        WeDialog.loading(this.context as FragmentActivity)

        RetrofitClient.chatCommand(Globals.currentEmployeeDetails.org_id, Globals.currentEmployeeDetails.id, Globals.currentEmployeeDetails.user_id, command, "{\"board_id\":${args.board_id}, \"column_id\":${args.column_id}}",
                object : Callback<ChatCommandResponseModel> {

                    override fun onResponse(call: Call<ChatCommandResponseModel>, response: Response<ChatCommandResponseModel>) {

                        val responseModel = response.body()

                        when {
                            response.isSuccessful && responseModel != null -> {
                                WeDialog.dismiss()

                                Globals.chatCommandResponse = responseModel.command_response

                                Log.e("chat command response ", "" + responseModel.command_response.success)
                                Log.e("chat command params ", "org_id " + Globals.currentEmployeeDetails.org_id)
                                Log.e("chat command params ", "employee id " + Globals.currentEmployeeDetails.id)
                                Log.e("chat command params ", "user_id " + Globals.currentEmployeeDetails.user_id)
                                Log.e("chat command params ", "args $command")
                                Log.e("chat command params ", "command " + "{\"board_id\":${args.board_id}, \"column_id\":${args.column_id}}")

                                var cardID: String = responseModel.command_response.card.id.toString()
                                        ?: ""

                                Log.e("new task", "${OrgURl}board/${board.project_id}/${board.slug}#$cardID")

                                bottomSheet.dismiss()

                                cl!!.messages.sendMessage(Message.options().withBody("${OrgURl}board/${board.project_id}/${board.slug}#$cardID"),
                                        ChatCallbackListener<Message>() {

                                        })

                                // Toastie.success(bottomSheet.context as FragmentActivity, "Successfully Created Task", Toast.LENGTH_SHORT).show()
                                advanceLastConsumedMessage()

                            }
                            else -> {
                                //  Toastie.warning(bottomSheet.context as FragmentActivity, "Error when Creating Task", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }

                    override fun onFailure(call: Call<ChatCommandResponseModel>, t: Throwable) {
                        //Toastie.error(bottomSheet.context as FragmentActivity, "Create Task Failed, Check your connection", Toast.LENGTH_SHORT).show()
                    }
                })
    }


    private fun advanceLastConsumedMessage() {
        var currentMember: Member? = cl!!.members.getMember(Globals.currentEmployeeDetails.email)
        if (cl!!.members != null && currentMember != null && cl!!.lastMessageIndex != null) {
            currentMember!!.channel!!.messages.setLastConsumedMessageIndexWithResult(cl!!.lastMessageIndex + 1,
                    object : CallbackListener<Long>() {
                        override fun onSuccess(value: Long?) {
                            Log.e("MessageActivity", "setLastConsumedMessageIndexWithResult callback " + value.toString())
                        }
                    })
        }
    }


    override fun getStatusBarColor() = Color.RED
    override fun getCornerRadius() = context!!.resources.getDimension(R.dimen.demo_sheet_rounded_corner)
    override fun getExpandedHeight() = -1

}


class CreateTicketSheetFragment(ticketText: String?, AccList: ArrayList<AccountsDetailsModel>?, context: Context, channel: Channel?) : SuperBottomSheetFragment() {
    var al = AccList
    var ctxt = context
    var tt = ticketText
    var cl = channel
    var OrgURl: String = "https://${Globals.currentEmployeeDetails?.organization_slug}.wittymanager.com"
            ?: ""
    var dueDate: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var v = inflater.inflate(R.layout.bottom_select_accounts_panel_layout, container, false)
        v.selectAcctitle.text = "Create New Ticket"
        v.dueTitle.text = "Set Due Date & Time"

        v.selectAccSpinner.setTitle("Select Account")
        v.selectAccSpinner.setPositiveButton("OK")

        val accSpinnerList = ArrayList<String>()

        al?.forEach {
            accSpinnerList.add("${it.name} (${it.code})")
        }

        val accSpinnerAdapter = ArrayAdapter(
                v.context, android.R.layout.simple_spinner_item, accSpinnerList
        )

        v.selectAccSpinner.adapter = accSpinnerAdapter


        v.confirmBtn.setOnClickListener {
            al?.forEach {
                if ("${it.name} (${it.code})" == v.selectAccSpinner.selectedItem.toString()) {
                    if(dueDate != ""){
                        Log.e("acc ID", "$it.account_id")
                        createTicket(tt, it.code, it.account_id, 1, Globals.currentEmployeeDetails.user_id, this, dueDate, "")
                        this.dismiss()
                    }else{
                        Toastie.warning(this.context as FragmentActivity, "Due Date is Required", Toast.LENGTH_SHORT).show()
                    }

                }
            }

        }

        v.setDueBtn.setOnClickListener {
            CustomDateTimePicker(this.context as FragmentActivity, object : CustomDateTimePicker.ICustomDateTimeListener {
                @SuppressLint("BinaryOperationInTimber")
                override fun onSet(
                        dialog: Dialog,
                        calendarSelected: Calendar,
                        dateSelected: Date,
                        year: Int,
                        monthFullName: String,
                        monthShortName: String,
                        monthNumber: Int,
                        day: Int,
                        weekDayFullName: String,
                        weekDayShortName: String,
                        hour24: Int,
                        hour12: Int,
                        min: Int,
                        sec: Int,
                        AM_PM: String
                ) {
                    //Get any time of date and time data here and process further...
                    val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    val date: Date = dateFormat.parse("$year-${monthNumber + 1}-$day $hour24:$min:00")
                    val selectedDate: String = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date.time)

                    dueDate = selectedDate
                    if (min == 0) {
                        v.setDueBtn.text = "$year-${monthNumber + 1}-$day $hour12 $AM_PM"
                    } else v.setDueBtn.text = "$year-${monthNumber + 1}-$day $hour12:$min $AM_PM"


                }

                override fun onCancel() {
                }
            }).apply {
                if (dueDate != ""){
                    val format = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
                    val date: Date = format.parse(dueDate)
                    setDate(date)
                }else {
                    val format = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
                    val currentDate: String = SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(Calendar.getInstance().time)
                    setDate(format.parse(currentDate))
                }//date and time will show in dialog is current time and date. We can change this according to our need

                showDialog()
            }
        }




        super.onCreateView(inflater, container, savedInstanceState)
        return v
    }

    fun dateToUTC(date: Date): Date? {
        return Date(date.time - Calendar.getInstance().timeZone.getOffset(date.time))
    }
    private fun createTicket(text: String?, code: String, account_id: Int, ticket_type_id: Int, createdBy: Int, bottomSheet: SuperBottomSheetFragment, dueDate: String, description: String) {


        val currentDate: String = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dateToUTC(Calendar.getInstance().time))
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val date: Date = dateFormat.parse(dueDate)
        val dueDateConverted: String = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dateToUTC(date))

        Log.e("Start-end date", "$currentDate-$dueDateConverted")

        WeDialog.loading(this.context as FragmentActivity)

        RetrofitClient.createTicket(Globals.currentEmployeeDetails.org_id, account_id, currentDate, dueDateConverted, ticket_type_id, text.toString(), createdBy, description,
                object : Callback<CreateTicketResponseModel> {

                    override fun onResponse(call: Call<CreateTicketResponseModel>, response: Response<CreateTicketResponseModel>) {

                        val responseModel = response.body()

                        when {
                            response.isSuccessful && responseModel != null -> {
                                WeDialog.dismiss()

                                Log.e("create ticket response ", "" + responseModel.success)
                                var series: String = responseModel.series_no
                                        ?: ""
                                Log.e("new task", "$OrgURl/support/ticket?id= m  ${code}-$series")

                                bottomSheet.dismiss()

                                cl!!.messages.sendMessage(Message.options().withBody("$OrgURl/support/ticket?id=${code}-$series"),
                                        ChatCallbackListener<Message>() {

                                        })

                                // Toastie.success(bottomSheet.context as FragmentActivity, "Successfully Created Task", Toast.LENGTH_SHORT).show()
                                advanceLastConsumedMessage()

                            }
                            else -> {
                                //  Toastie.warning(bottomSheet.context as FragmentActivity, "Error when Creating Task", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }

                    override fun onFailure(call: Call<CreateTicketResponseModel>, t: Throwable) {
                        //Toastie.error(bottomSheet.context as FragmentActivity, "Create Task Failed, Check your connection", Toast.LENGTH_SHORT).show()
                    }
                })
    }

    private fun advanceLastConsumedMessage() {
        var currentMember: Member? = cl!!.members.getMember(Globals.currentEmployeeDetails.email)
        if (cl!!.members != null && currentMember != null && cl!!.lastMessageIndex != null) {
            currentMember!!.channel!!.messages.setLastConsumedMessageIndexWithResult(cl!!.lastMessageIndex + 1,
                    object : CallbackListener<Long>() {
                        override fun onSuccess(value: Long?) {
                            Log.e("MessageActivity", "setLastConsumedMessageIndexWithResult callback " + value.toString())
                        }
                    })
        }
    }

    override fun getStatusBarColor() = Color.RED
    override fun getCornerRadius() = context!!.resources.getDimension(R.dimen.demo_sheet_rounded_corner)
    override fun getExpandedHeight() = -1


}




