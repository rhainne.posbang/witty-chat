package com.twilio.chat.witty.views

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.util.Base64
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat.startActivity
import com.github.thunder413.datetimeutils.DateTimeStyle
import com.github.thunder413.datetimeutils.DateTimeUnits
import com.github.thunder413.datetimeutils.DateTimeUtils
import com.github.tntkhang.fullscreenimageview.library.FullScreenImageViewActivity
import com.twilio.chat.*
import com.twilio.chat.witty.Globals
import com.twilio.chat.witty.R
import com.twilio.chat.witty.TwilioApplication
import com.twilio.chat.witty.activities.ChannelActivity
import com.twilio.chat.witty.activities.LoginActivity
import com.twilio.chat.witty.activities.PlaceHolderActivity
import com.twilio.chat.witty.activities.ProfileActivity
import com.twilio.chat.witty.models.MessageListModel
import eu.inloop.simplerecycleradapter.SettableViewHolder
import kotterknife.bindView
import org.jetbrains.anko.startActivity
import java.io.File
import java.io.FileOutputStream
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MessageViewHolder(val context: Context, parent: ViewGroup) : SettableViewHolder<MessageListModel>(context, R.layout.message_item_layout, parent) {
    val avatarView: ImageView by bindView(R.id.avatar)
    val reachabilityView: ImageView by bindView(R.id.reachability)
    val personBody: TextView by bindView(R.id.personBody)
    val mediaView: ImageView by bindView(R.id.mediaPreview)
    val mediaPreviewContainer: LinearLayout by bindView(R.id.mediaPreviewContainer)
    val author: TextView by bindView(R.id.author)
    val date: TextView by bindView(R.id.date)
    val identities: RelativeLayout by bindView(R.id.consumptionHorizonIdentities)
    val lines: LinearLayout by bindView(R.id.consumptionHorizonLines)
    val singleMessageContainer: LinearLayout by bindView(R.id.singleMessageContainer)
    val imageLoad: ProgressBar by bindView(R.id.imageLoad)

    override fun setData(message: MessageListModel) {
        val msg = message.message.message



        if (msg.author.compareTo(message.message.currentUser) == 0 || msg.author == "") {
            addAuthoNames(message, true)
            personBody.setBackgroundResource(R.drawable.you_chatb_g)
            personBody.setTextColor(Color.parseColor("#ffffff"))
            singleMessageContainer.gravity = Gravity.END
            mediaPreviewContainer.setBackgroundResource(R.drawable.you_chatb_g)
        } else {
            addAuthoNames(message, false)
            personBody.setBackgroundResource(R.drawable.person_chat_bg)
            personBody.setTextColor(Color.parseColor("#18181B"))
            singleMessageContainer.gravity = Gravity.START
            mediaPreviewContainer.setBackgroundResource(R.drawable.person_chat_bg)
        }

        for (member in message.message.members.membersList) {

            member.getAndSubscribeUser(object : CallbackListener<User>() {
                override fun onSuccess(user: User) {
                    Log.e("isOnline", "${user.identity} " + user.isOnline)
                }
            })

            if (msg.author.contentEquals(member.identity)) {

                fillUserAvatar(avatarView, member)
                // fillUserReachability(reachabilityView, member)

            }

            if (member.lastConsumedMessageIndex != null && member.lastConsumedMessageIndex == message.message.message.messageIndex) {
                drawConsumptionHorizon(member)
            }

        }
        if (msg.hasMedia()) {
            personBody.visibility = View.GONE
            imageLoad.visibility = View.VISIBLE
            Log.e("Media type", "${msg.media.type}")
            val file = File(context.cacheDir, msg.media.sid)
            if (file.exists()) {
                mediaPreviewContainer.visibility = View.VISIBLE
                mediaView.setImageURI(Uri.fromFile(File(context.cacheDir, msg.media.sid)))
                imageLoad.visibility = View.GONE
            } else {
                if (msg.media.type!!.contentEquals("image/jpeg") || msg.media.type!!.contentEquals("image/png")) {
                    val outStream = FileOutputStream(File(context.cacheDir, msg.media.sid))
                    msg.media.download(outStream, object : StatusListener() {
                        override fun onSuccess() {
                            Log.e("Media onSuccess", "Downloaded media")
                        }

                        override fun onError(error: ErrorInfo?) {
                            Log.e("Media onError", "$error")
                        }
                    }, object : ProgressListener() {
                        override fun onStarted() {
                            Log.e("Media onStarted", "Download Started")
                        }

                        override fun onProgress(bytes: Long) {
                            Log.e("Media onProgress", "Download Progressing")
                        }

                        override fun onCompleted(mediaSid: String?) {
                            Log.e("Media onCompleted", "$mediaSid")
                            Log.e("Media location", "${Uri.fromFile(File(context.cacheDir, mediaSid))}")
                            mediaPreviewContainer.visibility = View.VISIBLE
                            mediaView.setImageURI(Uri.fromFile(File(context.cacheDir, mediaSid)))
                            imageLoad.visibility = View.GONE
                        }
                    })
                }
            }

            mediaView.setOnClickListener {
                var uriString :  ArrayList<String> = ArrayList()
                uriString.add(Uri.fromFile(File(context.cacheDir, msg.media.sid)).toString())
                var fullImageIntent = Intent( context.applicationContext, FullScreenImageViewActivity::class.java)
                fullImageIntent.putExtra(FullScreenImageViewActivity.URI_LIST_DATA,uriString )
                fullImageIntent.putExtra(FullScreenImageViewActivity.IMAGE_FULL_SCREEN_CURRENT_POS, 0)
                startActivity(context,fullImageIntent,null)
            }

        } else {
            personBody.visibility = View.VISIBLE
            mediaPreviewContainer.visibility = View.GONE
        }

        author.text = getEmployeeName(msg.author)
        personBody.text = msg.messageBody
        date.text = getProperDate(msg.dateCreatedAsDate)

    }

    fun addAuthoNames(message: MessageListModel, isCurrentUser: Boolean) {
        val msg = message.message.message

        if ((message.message.message.messageIndex - 1) < 0) {
            if (message.group) {
                author.visibility = View.VISIBLE
            } else author.visibility = View.GONE
        } else {
            message.message.message.messages.getMessageByIndex(message.message.message.messageIndex - 1, object : CallbackListener<Message>() {
                override fun onSuccess(previousMessage: Message?) {
                    //Log.e("compare","previous ${previousMessage!!.member.identity} - ${previousMessage!!.messageBody} : current ${msg!!.author} - ${msg!!.messageBody}")
                    //Log.e("current","${msg!!.author} : ${msg!!.messageBody}")

                    if (msg.author.compareTo(previousMessage!!.author) == 0) {
                        author.visibility = View.GONE

                    } else {
                        if (message.group && !isCurrentUser) {
                            author.visibility = View.VISIBLE
                        } else author.visibility = View.GONE


                    }
                }
            })
        }
    }

    fun getEmployeeName(email: String): String {
        var employeName = ""
        Globals.contactList.forEach {
            if (it.email == email) {
                employeName = "${it.first_name} ${it.last_name}"
            }
        }
        return employeName
    }

    fun toggleDateVisibility() {
        date.visibility = if (date.visibility == View.GONE) View.VISIBLE else View.GONE
    }

    fun formatDate(dateToFormat: String, inputFormat: String, outputFormat: String): String {
        var convertedDate: String = SimpleDateFormat(outputFormat).format(SimpleDateFormat(inputFormat).parse(dateToFormat))
        return convertedDate;
    }


    fun getProperDate(dateRaw: Date): String {

        var isYesterday: Boolean = DateTimeUtils.isYesterday(dateRaw)
        val isToday = DateTimeUtils.isToday(dateRaw)
        val dateFormat = SimpleDateFormat("MM/dd/yy")
        val dateDisplay = dateFormat.format(dateRaw)
        val timeFormat: DateFormat = SimpleDateFormat("hh:mm a")
        val timeDisplay = timeFormat.format(dateRaw.time)
        val diff: Int = DateTimeUtils.getDateDiff(dateRaw, Date(), DateTimeUnits.HOURS)
        Log.e("date", "" + date)
        Log.e("date raw ", "" + dateRaw)
        return when {
            isYesterday -> {
                "Yesterday $timeDisplay"
            }
            isToday -> {
                when {
                    diff >= 5 -> {
                        "Today $timeDisplay"
                    }
                    else -> DateTimeUtils.getTimeAgo(itemView.context, dateRaw, DateTimeStyle.AGO_SHORT_STRING)
                }
            }
            else -> "$dateDisplay  $timeDisplay"
        }


    }

    private fun drawConsumptionHorizon(member: Member) {
        val ident = member.identity
        val color = getMemberRgb(ident)


        val identity = TextView(itemView.context)
        identity.text = ident
        identity.textSize = 8f
        identity.setTextColor(color)

        // Layout
        val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        val cc = identities.childCount
        if (cc > 0) {
            params.addRule(RelativeLayout.RIGHT_OF, identities.getChildAt(cc - 1).id)
        }
        identity.layoutParams = params

        val line = View(itemView.context)
        line.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 5)
        line.setBackgroundColor(color)

        identities.addView(identity)
        lines.addView(line)
    }

    private fun fillUserAvatar(avatarView: ImageView, member: Member) {
        TwilioApplication.instance.basicClient.chatClient?.users?.getAndSubscribeUser(member.identity, object : CallbackListener<User>() {
            override fun onSuccess(user: User) {
                val attributes = user.attributes
                val avatar = attributes.jsonObject?.opt("avatar") as String?
                if (avatar != null) {
                    val data = Base64.decode(avatar, Base64.NO_WRAP)
                    val bitmap = BitmapFactory.decodeByteArray(data, 0, data.size)
                    avatarView.setImageBitmap(bitmap)
                } else {
                    avatarView.setImageResource(R.drawable.avatar2)
                }
            }
        })
    }

    private fun fillUserReachability(reachabilityView: ImageView, member: Member) {
        if (!TwilioApplication.instance.basicClient.chatClient?.isReachabilityEnabled!!) {
            reachabilityView.setImageResource(R.drawable.reachability_disabled)
        } else {
            member.getAndSubscribeUser(object : CallbackListener<User>() {
                override fun onSuccess(user: User) {
                    if (user.isOnline) {
                        reachabilityView.setImageResource(R.drawable.reachability_online)
                    } else if (user.isNotifiable) {
                        reachabilityView.setImageResource(R.drawable.reachability_notifiable)
                    } else {
                        reachabilityView.setImageResource(R.drawable.reachability_offline)
                    }
                }
            })
        }


    }

    fun getMemberRgb(identity: String): Int {
        return HORIZON_COLORS[Math.abs(identity.hashCode()) % HORIZON_COLORS.size]
    }

    companion object {
        private val HORIZON_COLORS = intArrayOf(Color.GRAY, Color.RED, Color.BLUE, Color.GREEN, Color.MAGENTA)
    }
}
