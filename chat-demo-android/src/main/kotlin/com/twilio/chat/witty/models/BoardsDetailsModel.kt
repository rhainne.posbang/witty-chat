package com.twilio.chat.witty.models

import com.twilio.chat.witty.ChannelModel

data class BoardsDetailsModel(val id: Int, val project_id:Int, val slug :String, val title :String,val is_sprint_board :Int, val columns: ArrayList<ColumnsDetailsModel>)
