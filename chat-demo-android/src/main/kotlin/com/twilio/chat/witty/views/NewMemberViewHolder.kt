package com.twilio.chat.witty.views

import android.content.Context
import com.twilio.chat.witty.R
import com.twilio.chat.Member
import android.view.ViewGroup
import android.widget.TextView
import com.twilio.chat.witty.models.ContactsDetailsModel
import kotterknife.bindView
import eu.inloop.simplerecycleradapter.SettableViewHolder

class NewMemberViewHolder : SettableViewHolder<ContactsDetailsModel> {
    private val memberIdentity: TextView by bindView(R.id.identity)
//    private val memberSid: TextView by bindView(R.id.member_sid)

    constructor(context: Context, parent: ViewGroup)
        : super(context, R.layout.member_item_layout, parent)
    {}

    override fun setData(data: ContactsDetailsModel) {
        memberIdentity.text = "${data.first_name} ${data.last_name}"
    }
}
