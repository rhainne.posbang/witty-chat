package com.twilio.chat.witty

import com.twilio.chat.witty.models.ChannelListModel
import com.twilio.chat.witty.models.ChatCommandDetailResponse
import com.twilio.chat.witty.models.CurrentEmployeeDetailsModel
import com.twilio.chat.witty.models.ContactsDetailsModel

object Globals {
    var baseUrl = "https://api.wittymanager.com/"
    //var baseUrl = "https://api.dev.wittymanager.com/"
    lateinit var currentEmployeeDetails: CurrentEmployeeDetailsModel
    var contactList: List<ContactsDetailsModel> = ArrayList()
    lateinit var chatCommandResponse: ChatCommandDetailResponse
    var currentEmployeeID: Int = 0
    var deepLinkSelectedUser: String = ""
    var NotificationUser: String = ""
    //var OrgURl: String = "https://posbang.dev.wittymanager.com/"
    var selectedOrg: Int = 0
}