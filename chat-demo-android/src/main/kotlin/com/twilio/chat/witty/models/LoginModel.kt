package com.twilio.chat.witty.models

data class LoginModel(val id: Int, val employee_id: Int, val user_name: String , val restricted: Boolean, val restriction_msg: String)
