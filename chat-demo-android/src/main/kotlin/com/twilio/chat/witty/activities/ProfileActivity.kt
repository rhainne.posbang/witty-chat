package com.twilio.chat.witty.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import com.bumptech.glide.Glide
import com.twilio.chat.witty.BasicChatClient
import com.twilio.chat.witty.Globals
import com.twilio.chat.witty.R
import com.twilio.chat.witty.TwilioApplication
import com.twilio.chat.witty.database.RoomDatabase
import kotlinx.android.synthetic.main.activity_account_menut.*


class ProfileActivity : AppCompatActivity() {

    private lateinit var basicClient: BasicChatClient
    lateinit var roomDatabase: RoomDatabase
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_menut)

        basicClient = TwilioApplication.instance.basicClient
        roomDatabase = Room.databaseBuilder(applicationContext, RoomDatabase::class.java, "maindb").allowMainThreadQueries().build()

        var firstName = Globals.currentEmployeeDetails.first_name ?: ""
        var middleName = Globals.currentEmployeeDetails.middle_initial ?: ""
        var lastName = Globals.currentEmployeeDetails.last_name ?: ""
        var email = Globals.currentEmployeeDetails.email ?: ""
        var org = Globals.currentEmployeeDetails.org_id ?: ""
        var userID = Globals.currentEmployeeDetails.id ?: ""
        var employeeID = Globals.currentEmployeeDetails.current_employee_id ?: ""
        var imagePath = Globals.currentEmployeeDetails.image_path ?: ""
        var imagePathFinal = ""

        if (imagePath.contains(".com")) {
            imagePathFinal = "https:$imagePath"
        } else imagePathFinal = "https://${Globals.currentEmployeeDetails.organization_slug}.wittymanager.com$imagePath"

        Glide.with(this@ProfileActivity).load("$imagePathFinal").placeholder(R.drawable.user_avatar).dontAnimate().into(user_profile_image)

        Log.e("image path", "$imagePathFinal")

        profile_name.text = "$firstName $middleName $lastName"
        //v.profile_email.text = "$email (org:$org) (employee:$employeeID) (user: $userID)"
        profile_email.text = "$email"

        backBtn.setOnClickListener {
            finish()
            super.onBackPressed()
        }

        logout.setOnClickListener {

            Globals.contactList.forEach {
                roomDatabase.dbDao().deleteAllEmployees(it)
            }
            roomDatabase.dbDao().deleteCurrentUser(Globals.currentEmployeeDetails)

            basicClient.shutdown()
            finish()
            startActivity(Intent(this@ProfileActivity, LoginActivity::class.java))

        }

    }


}
