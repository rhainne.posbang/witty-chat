package com.twilio.chat.witty.models

import com.twilio.chat.witty.ChannelModel

data class ColumnsDetailsModel(val id: Int, val board_id:Int, val name :String)
