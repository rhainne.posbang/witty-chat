package com.twilio.chat.witty.models

data class CurrentEmployeeModel(val success: Boolean, val record: CurrentEmployeeDetailsModel,
                                val message: String, val total: Int, val query: String)
