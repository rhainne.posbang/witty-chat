package com.twilio.chat.witty.models

data class CardResponseModel(val id: Int, var title : String)
