package com.twilio.chat.witty.models

import com.twilio.chat.Channel
import com.twilio.chat.witty.ChannelModel

data class ChannelListModel(val channelModel: ChannelModel,val channel: Channel, val channelName:String, val imagePath:String,val group: Boolean)
