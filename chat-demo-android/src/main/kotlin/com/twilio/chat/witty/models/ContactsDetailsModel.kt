package com.twilio.chat.witty.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "all_employees")
data class ContactsDetailsModel(@PrimaryKey val id: Int, val org_id: Int, val first_name:  String?, val last_name:
String?, val middle_name:  String?, val email : String?, val image_path : String?, val middle_initial:  String?)
