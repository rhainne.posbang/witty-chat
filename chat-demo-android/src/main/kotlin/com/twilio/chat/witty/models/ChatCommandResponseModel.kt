package com.twilio.chat.witty.models

data class ChatCommandResponseModel(val command_response: ChatCommandDetailResponse)
