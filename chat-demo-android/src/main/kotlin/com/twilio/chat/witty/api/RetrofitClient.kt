package com.twilio.chat.witty.api

import android.util.Log
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import com.twilio.chat.witty.Globals
import com.twilio.chat.witty.models.*
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.http.Field


object RetrofitClient {


    var BASE_URL = Globals.baseUrl

    var headerAuthorizationInterceptor: Interceptor = Interceptor { chain ->
        var request: Request = chain.request()
        val headers =
                request.headers().newBuilder().add("Authorization", "Bearer ").build()
        request = request.newBuilder().headers(headers).build()
        Log.e("API: ", "" + request)
        chain.proceed(request)
    }

    var okHttpClient: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(headerAuthorizationInterceptor) // This is used to add ApplicationInterceptor.
            .build()

    val instance: ApiManager by lazy {

        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()


        retrofit.create(ApiManager::class.java)
    }


    fun postLogin(username: String, password: String, callback: Callback<LoginModel>) {
        val userCall = instance.loginUser(username, password)
        userCall.enqueue(callback)
    }

    fun getCurrentEmployee(user_id: Int, callback: Callback<CurrentEmployeeModel>) {
        val userCall = instance.getCurrentEmployee(user_id)
        userCall.enqueue(callback)
    }

    fun getContacts(org_id: Int, callback: Callback<ContactsModel>) {
        val userCall = instance.getContacts(org_id)
        userCall.enqueue(callback)
    }

    fun chatCommand(org_id: Int,employee_id: Int,user_id: Int,command: String, args: String,callback: Callback<ChatCommandResponseModel>) {
        val userCall = instance.chatCommand(org_id,employee_id,user_id,command,args)
        userCall.enqueue(callback)
    }

    fun getBoards(org_id: Int, callback: Callback<BoardsModel>) {
        val userCall = instance.getBoards(org_id,100)
        userCall.enqueue(callback)
    }

    fun getOrg(id: Int, callback: Callback<OrgModel>) {
        val userCall = instance.getOrg(id)
        userCall.enqueue(callback)
    }

    fun getUserOrg(user_id: Int, callback: Callback<UserOrgModel>) {
        val userCall = instance.getUserOrg(user_id)
        userCall.enqueue(callback)
    }

    fun getAccounts(org_id: Int, callback: Callback<ArrayList<AccountsDetailsModel>>) {
        val userCall = instance.getAccounts(org_id)
        userCall.enqueue(callback)
    }


    fun createTicket(org_id: Int,acc_id: Int, date_start: String,date_due: String,ticket_type_id: Int,title: String,created_by: Int ,description: String,callback: Callback<CreateTicketResponseModel>) {
        val userCall = instance.createTicket(org_id,acc_id,date_start,date_due,ticket_type_id,title, created_by,description)
        userCall.enqueue(callback)
    }




}