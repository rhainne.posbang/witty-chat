package com.twilio.chat.witty.views

import ChatCallbackListener
import android.content.Context
import android.graphics.Typeface
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.facebook.shimmer.ShimmerFrameLayout
import com.github.thunder413.datetimeutils.DateTimeStyle
import com.github.thunder413.datetimeutils.DateTimeUnits
import com.github.thunder413.datetimeutils.DateTimeUtils
import com.twilio.chat.*
import com.twilio.chat.Channel.ChannelType
import com.twilio.chat.Channel.NotificationLevel
import com.twilio.chat.witty.Constants
import com.twilio.chat.witty.Globals
import com.twilio.chat.witty.R
import com.twilio.chat.witty.TwilioApplication
import com.twilio.chat.witty.activities.MessageActivity
import com.twilio.chat.witty.models.ChannelListModel
import de.hdodenhof.circleimageview.CircleImageView
import eu.inloop.simplerecycleradapter.SettableViewHolder
import kotterknife.bindView
import org.jetbrains.anko.*
import java.text.SimpleDateFormat
import java.util.*

class ChannelViewHolder : SettableViewHolder<ChannelListModel>, AnkoLogger {
    val friendlyName: TextView by bindView(R.id.channel_friendly_name)
    val channelSid: TextView by bindView(R.id.channel_sid)
    val updatedDate: TextView by bindView(R.id.channel_updated_date)
    val createdDate: TextView by bindView(R.id.channel_created_date)
    val usersCount: TextView by bindView(R.id.channel_users_count)
    val totalMessagesCount: TextView by bindView(R.id.channel_total_messages_count)
    val unconsumedMessagesCount: TextView by bindView(R.id.channel_unconsumed_messages_count)
    val lastMessageDate: TextView by bindView(R.id.channel_last_message_date)
    val pushesLevel: TextView by bindView(R.id.channel_pushes_level)
    val lastMessage: TextView by bindView(R.id.channel_last_message)
    val shimmerViewLastMessageContainer: ShimmerFrameLayout by bindView(R.id.shimmer_view_LastMessage_container)
    val reachabilityChannelBadge: ImageView by bindView(R.id.reachabilityChannelBadge)
    val profileImage: CircleImageView by bindView(R.id.profile_image)
    val unreadIndicator: LinearLayout by bindView(R.id.unread_indicator)
    val notifBadge: ImageView by bindView(R.id.notifBadge)

    constructor(context: Context, parent: ViewGroup)
            : super(context, R.layout.channel_item_layout, parent) {
    }

    override fun setData(channel: ChannelListModel) {
        warn { "setData for ${channel.channel.friendlyName} sid|${channel.channel.sid}|" }
        var context = itemView.context
        var imagePathFinal = ""

        if (!channel.group) {

            if (channel.imagePath.contains(".com")) {
                imagePathFinal = "https:${channel.imagePath}"
            } else imagePathFinal = "https://${Globals.currentEmployeeDetails.organization_slug}.wittymanager.com${channel.imagePath}"

            Glide.with(context).load("$imagePathFinal").placeholder(R.drawable.user_avatar).dontAnimate().into(profileImage)

            Log.e("image path", "$imagePathFinal")

        } else Glide.with(context).load("$imagePathFinal").placeholder(R.drawable.group_avatar).dontAnimate().into(profileImage)

        reachabilityChannelBadge.setImageResource(R.drawable.reachability_disabled)

        if (channel.channel.lastMessageIndex?.toInt() != null) {
            channel.channel.messages?.getMessageByIndex(channel.channel.lastMessageIndex, ChatCallbackListener<Message>() { message ->
                //Log.e("getMessageByIndex", "" + message.messageBody)
                shimmerViewLastMessageContainer.visibility = View.GONE
                lastMessage.visibility = View.VISIBLE
                lastMessage.text = message.messageBody

            })


            /*      doAsync {
                      channel.channel.getLastMessage(channel.channel.lastMessageIndex!!.toInt(), object : CallbackListener<List<Message>>() {
                          override fun onSuccess(messages: List<Message>) {
                              for (message in messages) {
                                  uiThread {
                                      it.lastMessage.visibility = View.VISIBLE
                                      it.shimmerViewLastMessageContainer.visibility = View.GONE
                                      it.lastMessage.text = message.messageBody

                                  }
                              }
                          }
                      })

                  }*/
        } else {
            lastMessage.text = ""
            lastMessage.visibility = View.VISIBLE
            shimmerViewLastMessageContainer.visibility = View.GONE
        }

        friendlyName.text = channel.channelName
        channelSid.text = channel.channel.sid

        fun fillUserReachability(reachabilityChannelBadge: ImageView, member: Member?) {
            if (!TwilioApplication.instance.basicClient.chatClient?.isReachabilityEnabled!!) {
                reachabilityChannelBadge.setImageResource(R.drawable.reachability_disabled)
            } else {
                member!!.getAndSubscribeUser(object : CallbackListener<User>() {
                    override fun onSuccess(user: User) {
                        if (user.isOnline) {
                            reachabilityChannelBadge.setImageResource(R.drawable.reachability_online)

                        } else if (!user.isOnline) {
                            reachabilityChannelBadge.setImageResource(R.drawable.reachability_disabled)
                        }
                    }
                })
            }
        }

        //check if online
        val basicClient = TwilioApplication.instance.basicClient
        val channelSid = channel.channel.sid
        val channelsObject = basicClient.chatClient!!.channels

        if (!channel.group) {
            val handler = android.os.Handler()
            handler.postDelayed(object : Runnable {
                override fun run() {
                    //Call your function here
                    channelsObject.getChannel(channelSid, ChatCallbackListener<Channel>() {
                        val members: Int? = it.members?.membersList?.size ?: null

                        if (members != null) {
                            if (channel.channel!!.type == ChannelType.PRIVATE && members == 2) {
                                val channelName: List<String> = channel.channel!!.uniqueName.split("-")

                                if (channelName[0] != Globals.currentEmployeeDetails.email) {
                                    var member: Member? = it.members.getMember(channelName[0])
                                            ?: null
                                    if (member?.identity != null) {
                                        fillUserReachability(reachabilityChannelBadge, member)
                                    }

                                } else if (channelName[1] != Globals.currentEmployeeDetails.email) {
                                    var member: Member? = it.members.getMember(channelName[1])
                                            ?: null
                                    if (member?.identity != null) {
                                        fillUserReachability(reachabilityChannelBadge, member)
                                    }
                                }
                            } else reachabilityChannelBadge.setImageResource(R.drawable.reachability_disabled)
                        } else reachabilityChannelBadge.setImageResource(R.drawable.reachability_disabled)

                    })


                    handler.postDelayed(this, 5000)//5 sec delay
                }
            }, 1000)
        } else {
            reachabilityChannelBadge.setImageResource(R.drawable.reachability_disabled)
        }

        if (channel.channel.dateUpdatedAsDate != null) {
            updatedDate.text = DateTimeUtils.formatWithStyle("2017-06-13", DateTimeStyle.SHORT).toString()
        } else updatedDate.text = "<no updated date>"


        if (channel.channel.dateUpdatedAsDate != null) {
            createdDate.text = DateTimeUtils.formatWithStyle("2017-06-13", DateTimeStyle.SHORT).toString()
        } else createdDate.text = "<no updated date>"



        if (channel.channel.notificationLevel == NotificationLevel.MUTED){
            notifBadge.visibility = View.VISIBLE
        }else  notifBadge.visibility = View.GONE



        if (channel.channel.status == Channel.ChannelStatus.JOINED && channel.channel.members != null) {
            var currentMember: Member? = channel.channel!!.members.getMember(Globals.currentEmployeeDetails.email)
            /*if (channel.channel.lastMessageIndex != null) {
                if (currentMember!!.lastConsumedMessageIndex != null && currentMember!!.lastConsumedMessageIndex == channel.channel.lastMessageIndex) {
                    unreadIndicator.visibility = View.GONE
                    lastMessage.typeface = Typeface.DEFAULT
                    friendlyName.typeface = Typeface.DEFAULT
                } else {
                    unreadIndicator.visibility = View.VISIBLE
                    lastMessage.typeface = Typeface.DEFAULT_BOLD
                    friendlyName.typeface = Typeface.DEFAULT_BOLD
                }
            } else {
                unreadIndicator.visibility = View.GONE
                lastMessage.typeface = Typeface.DEFAULT
                friendlyName.typeface = Typeface.DEFAULT
            }
*/     if (channel.channel.members != null && currentMember != null) {
                currentMember!!.channel.getUnconsumedMessagesCount(object : CallbackListener<Long>() {
                    override fun onSuccess(value: Long?) {
                        Log.d("ChannelViewHolder", "getUnconsumedMessagesCount callback")
                        Log.e("UnconsumedMessagesCount", "" + value!!.toString())

                        if (value!!.toInt() >= 1) {
                            unreadIndicator.visibility = View.VISIBLE
                            lastMessage.typeface = Typeface.DEFAULT_BOLD
                            friendlyName.typeface = Typeface.DEFAULT_BOLD
                        } else {
                            unreadIndicator.visibility = View.GONE
                            lastMessage.typeface = Typeface.DEFAULT
                            friendlyName.typeface = Typeface.DEFAULT
                        }
                    }
                })
            }
        }


        channel.channel.getMessagesCount(object : CallbackListener<Long>() {
            override fun onSuccess(value: Long?) {
                Log.d("ChannelViewHolder", "getMessagesCount callback")
                totalMessagesCount.text = "Messages " + value!!.toString()
            }
        })

        channel.channel.getMembersCount(object : CallbackListener<Long>() {
            override fun onSuccess(value: Long?) {
                Log.d("ChannelViewHolder", "getMembersCount callback")
                usersCount.text = "Members " + value!!.toString()
            }
        })



        fun formatDate(dateToFormat: String, inputFormat: String, outputFormat: String): String {
            var convertedDate: String = SimpleDateFormat(outputFormat).format(SimpleDateFormat(inputFormat).parse(dateToFormat))
            return convertedDate;
        }

        var dateDisplay = channel.channel.lastMessageDate

        if (channel.channel.lastMessageDate == null) {
            dateDisplay = channel.channel.dateCreatedAsDate
        }
        val dateSplit: List<String> = dateDisplay.toString().split(" ")
        val month = formatDate(dateSplit[1], "MMM", "MM")
        val day = formatDate(dateSplit[2], "dd", "dd")
        val year = dateSplit[5]
        val time = formatDate(dateSplit[3], "HH:mm:ss", "HH:mm:ss a")
        val timeDisplay = formatDate(dateSplit[3], "HH:mm:ss", "hh:mm a")
        val messageDate = "$year-$month-$day"

        var isYesterday: Boolean = DateTimeUtils.isYesterday(messageDate)
        val isToday = DateTimeUtils.isToday(messageDate)
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss aa")
        val date: Date = format.parse(messageDate + " " + time)
        val diff: Int = DateTimeUtils.getDateDiff(date, Date(), DateTimeUnits.HOURS)

        when {
            isYesterday -> {
                lastMessageDate.text = "Yesterday"
            }
            isToday -> {
                Log.e("date", "$date")
                when {
                    diff >= 5 -> {

                        lastMessageDate.text = timeDisplay
                    }
                    else -> lastMessageDate.text = DateTimeUtils.getTimeAgo(itemView.context, date, DateTimeStyle.AGO_SHORT_STRING)
                }
            }
            else -> lastMessageDate.text = DateTimeUtils.formatWithStyle(messageDate, DateTimeStyle.SHORT)
        }


        /*itemView.setBackgroundColor(
            if (channel.status == ChannelStatus.JOINED) {
                if (channel.type == ChannelType.PRIVATE)
                    Color.BLUE
                else
                    Color.WHITE
            } else {
                if (channel.status == ChannelStatus.INVITED)
                    Color.YELLOW
                else
                    Color.GRAY
            }
        )*/
    }
}
