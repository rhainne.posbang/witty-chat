package com.twilio.chat.witty.models

data class OrgModel(val id: Int, val name:String, val slug:String)
