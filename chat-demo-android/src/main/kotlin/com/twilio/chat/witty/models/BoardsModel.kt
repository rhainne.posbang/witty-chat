package com.twilio.chat.witty.models

data class BoardsModel(val boards: ArrayList<BoardsDetailsModel>)
