package com.twilio.chat.witty

object FCMPreferences {
    val SENT_TOKEN_TO_SERVER = "sentTokenToServer"
    val REGISTRATION_COMPLETE = "registrationComplete"
}
