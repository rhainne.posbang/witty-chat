package com.twilio.chat.witty.models

data class ContactsModel(val success: Boolean, val record: ArrayList<ContactsDetailsModel>)
